<?php

    require_once $_SERVER['DOCUMENT_ROOT'] . "/resource/path.php";
    require_once $_SERVER["DOCUMENT_ROOT"] . PathFile::$CONNECT_DB_SWEAR_WORD;

    class TableDbRemovingExtraCharacters {

        // cn - column name
        private $cnExtraCharacter= "extra_character";

        private $nameTable = "removing_extra_characters";
        private $link;

        public function __construct(){
            $this->link = ConnectDbSwearWord::connect();
        }

        public function getSymbolBySymbol($symbol){
            $query = "SELECT * FROM `$this->nameTable` WHERE `$this->cnExtraCharacter`=?";
            $res = $this->link->prepare($query);
            $res->bind_param("s", $symbol);
            $res->execute();
            return $res->get_result()->fetch_all(MYSQLI_ASSOC);
        }
        
        public function getCnExtraCharacter(): string{
            return $this->cnExtraCharacter;
        }
        
    }