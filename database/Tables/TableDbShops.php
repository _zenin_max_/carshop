<?php
    require_once $_SERVER['DOCUMENT_ROOT'] . "/resource/path.php";
    require_once $_SERVER["DOCUMENT_ROOT"] . PathFile::$CONNECT_DB_CARSHOP;

    class TableDbShops{

        // cn - column name
        private $cnId= "id";
        private $cnAdress = "adress";
        private $cnCoordinates = "coordinates";
        private $cnTimetable = "timetable";

        private $nameTable = "shops";
        private $link;

        public function __construct($id = null){
            $this->link = ConnectDbCarshop::connect();
        }

        public function getAllShops(){
            $query = "SELECT * FROM `$this->nameTable`";
            $res = $this->link->prepare($query);
            $res->execute();
            return $res->get_result()->fetch_all(MYSQLI_ASSOC);
        }

        public function getShopById($idShop){
            $query = "SELECT * FROM `$this->nameTable` WHERE `$this->cnId`=?";
            $res = $this->link->prepare($query);
            $res->bind_param("s", $idShop);
            $res->execute();
            return $res->get_result()->fetch_all(MYSQLI_ASSOC)[0];
        }

        public function getCnId(){
            return $this->cnId;
        }

        public function getCnAdress(){
            return $this->cnAdress;
        }

        public function getCnCoordinates(){
            return $this->cnCoordinates;
        }

        public function getCnTimetable(){
            return $this->cnTimetable;
        }
    }
