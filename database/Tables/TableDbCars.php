<?php

    require_once $_SERVER['DOCUMENT_ROOT'] . "/resource/path.php";
    require_once $_SERVER["DOCUMENT_ROOT"] . PathFile::$CONNECT_DB_CARSHOP;
    require_once $_SERVER["DOCUMENT_ROOT"] . PathFile::$CALLBACK_OPERATION;
    require_once $_SERVER["DOCUMENT_ROOT"] . PathFile::$STRING;

    class TableDbCars {

        // cn - column name
        private $cnId = "id";
        private $cnIdBrand = "id_brand";
        private $cnNameCar = "name_car";
        private $cnAccelerationTo100 = "acceleration_to_100";
        private $cnMaximumSpeed = "maximum_speed";
        private $cnCountryOfManufacture = "country_of_manufacture";

        private $nameTable = "cars";
        private $link;

        public function __construct(){
            $this->link = ConnectDbCarshop::connect();
        }

        public function getListAllCars(){
            $query = "SELECT * FROM `$this->nameTable`";
            $res = $this->link->prepare($query);
            $res->execute();
            return $res->get_result()->fetch_all(MYSQLI_ASSOC);
        }

        public function getListCarsByIdBrand($idBrand){
            $query = "SELECT * FROM `$this->nameTable` WHERE `$this->cnIdBrand`=?";
            $res = $this->link->prepare($query);
            $res->bind_param("s", $idBrand);
            $res->execute();
            return $res->get_result()->fetch_all(MYSQLI_ASSOC);
        }

        public function getCarById($idCar){
            $query = "SELECT * FROM `$this->nameTable` WHERE `$this->cnId`=?";
            $res = $this->link->prepare($query);
            $res->bind_param("s", $idCar);
            $res->execute();
            return $res->get_result()->fetch_all(MYSQLI_ASSOC)[0];
        }

        public function insertCar($idBrand, $nameCar, $accelerationTo100, $maximumSpeed, $countryOfManufacture,
                                  CallbackOperation $callbackOperation){
            $query = "INSERT INTO `$this->nameTable`(`$this->cnIdBrand`, `$this->cnNameCar`, `$this->cnAccelerationTo100`,"
                . " `$this->cnMaximumSpeed`, `$this->cnCountryOfManufacture`)  VALUES (?, ?, ?, ?, ?)";
            $res = $this->link->prepare($query);
            $res->bind_param("sssss", $idBrand, $nameCar, $accelerationTo100, $maximumSpeed, $countryOfManufacture);
            if ($res->execute()){
                ($callbackOperation->onSuccess)(Status::$SUCCESS, "Машина добавлена в БД");
            } else {
                ($callbackOperation->onFail)(Status::$ERROR_DATABASE, "Машина не была добавлена в БД" . $res->error);
            }
        }

        public function updateCar($id, $idBrand, $nameCar, $accelerationTo100, $maximumSpeed,
                                  $countryOfManufacture, CallbackOperation $callbackOperation){
            $query = "UPDATE `$this->nameTable` SET `$this->cnId`=?,`$this->cnIdBrand`=?,`$this->cnNameCar`=?,"
                . "`$this->cnAccelerationTo100`=?,`$this->cnMaximumSpeed`=?,"
                . "`$this->cnCountryOfManufacture`=? WHERE `$this->cnId`=?";
            $res = $this->link->prepare($query);
            $res->bind_param("sssssss", $id, $idBrand, $nameCar, $accelerationTo100, $maximumSpeed, $countryOfManufacture, $id);
            if ($res->execute()){
                ($callbackOperation->onSuccess)(Status::$SUCCESS, "Машина обновлена в БД");
            } else {
                ($callbackOperation->onFail)(Status::$ERROR_DATABASE, "Машина не была добновлена в БД: " . $res->error);
            }
        }

        public function deleteCar($id, CallbackOperation $callbackOperation){
            $query = "DELETE FROM `$this->nameTable` WHERE `$this->cnId`=?";
            $res = $this->link->prepare($query);
            $res->bind_param("s", $id);
            if ($res->execute()){
                ($callbackOperation->onSuccess)(Status::$SUCCESS, "Машина удалена из БД");
            } else {
                ($callbackOperation->onFail)(Status::$ERROR_DATABASE, "Машина не была удалена из БД: " . $res->error);
            }
        }

        public function deleteAllCarsByIdCarBrand($idBrand){
            $query = "DELETE FROM `$this->nameTable` WHERE `$this->cnIdBrand`=?";
            $res = $this->link->prepare($query);
            $res->bind_param("s", $idBrand);
            $res->execute();
        }

        public function searchCarsByWord($word){
            $word = "%$word%";
            $query = "SELECT * FROM `$this->nameTable` where concat(`$this->cnNameCar`,`$this->cnAccelerationTo100`,`$this->cnMaximumSpeed`, `$this->cnCountryOfManufacture`) like ?";
            $res = $this->link->prepare($query);
            $res->bind_param("s", $word);
            $res->execute();
            return $res->get_result()->fetch_all(MYSQLI_ASSOC);
        }


        public function getCnId(){
            return $this->cnId;
        }

        public function getCnIdBrand(){
            return $this->cnIdBrand;
        }

        public function getCnNameCar(){
            return $this->cnNameCar;
        }

        public function getCnAccelerationTo100(){
            return $this->cnAccelerationTo100;
        }

        public function getCnMaximumSpeed(){
            return $this->cnMaximumSpeed;
        }

        public function getCnCountryOfManufacture(){
            return $this->cnCountryOfManufacture;
        }

    }