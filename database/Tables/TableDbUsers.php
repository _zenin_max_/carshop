<?php
    require_once $_SERVER['DOCUMENT_ROOT'] . "/resource/path.php";
    require_once $_SERVER["DOCUMENT_ROOT"] . PathFile::$CONNECT_DB_CARSHOP;

    class TableDbUsers {

        // cn - column name
        private $cnId= "id";
        private $cnLogin= "login";
        private $cnPassword= "password";
        private $cnName= "name";
        private $cnSurname= "surname";
        private $cnMail= "mail";
        private $cnFavoriteCars= "favorite_cars";
        private $cnPrivilege= "privilege";

        private $nameTable = "users";
        private $link;

        public function __construct(){
            $this->link = ConnectDbCarshop::connect();
        }

        public function getUserByLogin($login){
            $query = "SELECT * FROM `$this->nameTable` WHERE `$this->cnLogin`=?";
            $res = $this->link->prepare($query);
            $res->bind_param("s", $login);
            $res->execute();
            return $res->get_result()->fetch_all(MYSQLI_ASSOC);
        }

        public function getUserById($id){
            $query = "SELECT * FROM `$this->nameTable` WHERE `$this->cnId`=?";
            $res = $this->link->prepare($query);
            $res->bind_param("s", $id);
            $res->execute();
            return $res->get_result()->fetch_all(MYSQLI_ASSOC)[0];
        }

        public function updateUser($idUser, $login, $name, $surname, $mail, $favoriteCars){
            $query = "UPDATE `$this->nameTable` SET `$this->cnLogin`=?, `$this->cnName`=?, `$this->cnSurname`=?, `$this->cnMail`=?,`$this->cnFavoriteCars`=? WHERE `id`=?";
            $res = $this->link->prepare($query);
            $res->bind_param("ssssss", $login, $name, $surname, $mail, $favoriteCars, $idUser);
            $res->execute();
        }

        public function insertNewUser($login, $password, $name, $surname, $mail, CallbackOperation $callbackOperation){
            $query = "INSERT INTO `$this->nameTable` ( `$this->cnLogin`, `$this->cnPassword`, `$this->cnName`, `$this->cnSurname`, `$this->cnMail`) VALUES (?, ?, ?, ?, ?)";
            $res = $this->link->prepare($query);
            $res->bind_param("sssss", $login, $password, $name, $surname, $mail);
            if ($res->execute()){
                ($callbackOperation->onSuccess)("Пользователь добавлен в БД.");
            } else {
                ($callbackOperation->onFail)(Status::$ERROR_DATABASE, "Пользователь не был добавлен в БД: " . $res->error);
            }
        }

        public function getCnId(): string{
            return $this->cnId;
        }

        public function getCnLogin(): string{
            return $this->cnLogin;
        }

        public function getCnPassword(): string{
            return $this->cnPassword;
        }

        public function getCnName(): string{
            return $this->cnName;
        }

        public function getCnSurname(): string{
            return $this->cnSurname;
        }

        public function getCnMail(): string{
            return $this->cnMail;
        }

        public function getCnFavoriteCars(): string{
            return $this->cnFavoriteCars;
        }

        public function getCnPrivilege(){
            return $this->cnPrivilege;
        }

    }

?>
