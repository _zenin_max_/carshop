<?php

    require_once $_SERVER['DOCUMENT_ROOT'] . "/resource/path.php";
    require_once $_SERVER["DOCUMENT_ROOT"] . PathFile::$CONNECT_DB_CARSHOP;

    class TableDbReviewsAboutCar {

        // cn - column name
        private $cnId = "id";
        private $cnIdUser = "id_user";
        private $cnReview = "review";
        private $cnRating = "rating";
        private $cnDate = "date";
        private $cnIdCar = "id_car";

        private $nameTable = "reviews_about_cars";
        private $link;

        public function __construct(){
            $this->link = ConnectDbCarshop::connect();
        }

        public function getReviewById($id){
            $query = "SELECT * FROM `$this->nameTable` WHERE `$this->cnId`=?";
            $res = $this->link->prepare($query);
            $res->bind_param("s", $id);
            $res->execute();
            return $res->get_result()->fetch_all(MYSQLI_ASSOC)[0];
        }

        public function getListReviewByIdCar($idCar){
            $query = "SELECT * FROM `$this->nameTable` WHERE `$this->cnIdCar`=?";
            $res = $this->link->prepare($query);
            $res->bind_param("s", $idCar);
            $res->execute();
            return $res->get_result()->fetch_all(MYSQLI_ASSOC);
        }

        public function insertReviewCar( $idUser, $review, $date, $rating, $idCar, CallbackOperation $callbackOperation){
            $query = "INSERT INTO `$this->nameTable` ( `$this->cnIdUser`, `$this->cnReview`, `$this->cnDate`, `$this->cnRating`, `$this->cnIdCar`) VALUES (?, ?, ?, ?, ?)";
            $res = $this->link->prepare($query);
            $res->bind_param("sssss", $idUser, $review, $date, $rating, $idCar);
            if ($res->execute()){
                ($callbackOperation->onSuccess)(Status::$SUCCESS, "Отзыв добавлен в БД");
            } else {
                ($callbackOperation->onFail)(Status::$ERROR_DATABASE, "Отзыв не был добавлен в БД: " . $res->error);
            }
        }

        public function updateReviewCar( $idReview, $review, $date, $rating, $idCar, CallbackOperation $callbackOperation){
            $query = "UPDATE `$this->nameTable` SET `$this->cnReview`=?, `$this->cnDate`=?, `$this->cnRating`=?, `$this->cnIdCar`=?  WHERE `$this->cnId`=?";
            $res = $this->link->prepare($query);
            $res->bind_param("sssss", $review, $date, $rating, $idCar, $idReview);
            if ($res->execute()){
                ($callbackOperation->onSuccess)(Status::$SUCCESS, "Отзыв обнавлён в БД");
            } else {
                ($callbackOperation->onFail)(Status::$ERROR_DATABASE, "Отзыв не был обновлён в БД: " . $res->error);
            }
        }

        public function deleteReviewCar( $idReview, CallbackOperation $callbackOperation){
            $query = "DELETE FROM `$this->nameTable` WHERE `$this->cnId`=?";
            $res = $this->link->prepare($query);
            $res->bind_param("s", $idReview);
            if ($res->execute()){
                ($callbackOperation->onSuccess)(Status::$SUCCESS, "Отзыв удалён из БД");
            } else {
                ($callbackOperation->onFail)(Status::$ERROR_DATABASE, "Отзыв не был удалён из БД" . $res->error);
            }
        }


        public function getCnId(){
            return $this->cnId;
        }

        public function getCnIdUser(){
            return $this->cnIdUser;
        }

        public function getCnReview(){
            return $this->cnReview;
        }

        public function getCnRating(){
            return $this->cnRating;
        }

        public function getCnDate(){
            return $this->cnDate;
        }

        public function getCnIdCar(): string{
            return $this->cnIdCar;
        }

    }