<?php
    require_once $_SERVER['DOCUMENT_ROOT'] . "/resource/path.php";
    require_once $_SERVER["DOCUMENT_ROOT"] . PathFile::$CONNECT_DB_CARSHOP;
    require_once $_SERVER["DOCUMENT_ROOT"] . PathFile::$CALLBACK_OPERATION;
    require_once $_SERVER["DOCUMENT_ROOT"] . PathFile::$STRING;

    class TableDbCarBrands{

        // cn - column name
        private $cnId= "id";
        private $cnNameBrand = "name-brand";
        private $cnAboutBrand = "about-brand";
        
        private $nameTable = "car-brands";
        private $link;
        
        public function __construct(){
            $this->link = ConnectDbCarshop::connect();
        }

        public function getListAllCarBrands(){
            $query = "SELECT * FROM `$this->nameTable`";
            $res = $this->link->prepare($query);
            $res->execute();
            return $res->get_result()->fetch_all(MYSQLI_ASSOC);
        }

        public function getCarBrandById($idCar){
            $query = "SELECT * FROM `$this->nameTable` WHERE `$this->cnId`=?";
            $res = $this->link->prepare($query);
            $res->bind_param("s", $idCar);
            $res->execute();
            return $res->get_result()->fetch_all(MYSQLI_ASSOC)[0];
        }

        public function insertCarBrand($nameBrand, $aboutBrand, CallbackOperation $callbackOperation){
            $query = "INSERT INTO `$this->nameTable`(`$this->cnNameBrand`, `$this->cnAboutBrand`) VALUES (?, ?)";
            $res = $this->link->prepare($query);
            $res->bind_param("ss", $nameBrand, $aboutBrand);
            if ($res->execute()){
                ($callbackOperation->onSuccess)( "Бренд добавлен в БД");
            } else {
                ($callbackOperation->onFail)(Status::$ERROR_DATABASE, "Бренд не был добавлен в БД:" . $res->error);
            }
        }

        public function updateCarBrand($id, $nameBrand, $aboutBrand, CallbackOperation $callbackOperation){
            $query = "UPDATE `$this->nameTable` SET `$this->cnId`=?,`$this->cnNameBrand`=?,`$this->cnAboutBrand`=?  WHERE `$this->cnId`=?";
            $res = $this->link->prepare($query);
            $res->bind_param("ssss", $id, $nameBrand, $aboutBrand, $id);
            if ($res->execute()){
                ($callbackOperation->onSuccess)(Status::$SUCCESS, "Бренд обновлён в БД");
            } else {
                ($callbackOperation->onFail)(Status::$ERROR_DATABASE, "Бренд не был обновлён в БД: " . $res->error);
            }
        }

        public function deleteCarBrands($id, CallbackOperation $callbackOperation){
            $query = "DELETE FROM `$this->nameTable` WHERE `$this->cnId`=?";
            $res = $this->link->prepare($query);
            $res->bind_param("s", $id);
            if ($res->execute()){
                ($callbackOperation->onSuccess)(Status::$SUCCESS, "Бренд удалён из БД");
            } else {
                ($callbackOperation->onFail)(Status::$ERROR_DATABASE, "Бренд не был удалён из БД: " . $res->error);
            }
        }

        public function getCnId(){
            return $this->cnId;
        }

        public function getCnNameBrand(){
            return $this->cnNameBrand;
        }

        public function getCnAboutBrand(){
            return $this->cnAboutBrand;
        }

    }