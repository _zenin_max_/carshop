<?php

    require_once $_SERVER['DOCUMENT_ROOT'] . "/resource/path.php";
    require_once $_SERVER["DOCUMENT_ROOT"] . PathFile::$CONNECT_DB_CARSHOP;

    class TableDbCategories {

        // cn - column name
        private $cnId= "id";
        private $cnName= "name_ru";
        private $cnNameForSystem= "name_for_system";

        private $nameTable = "categories";
        private $link;

        public function __construct(){
            $this->link = ConnectDbCarshop::connect();
        }

        function getListCategories(){
            $query = "SELECT * FROM `$this->nameTable`";
            $res = $this->link->prepare($query);
            $res->execute();
            return $res->get_result()->fetch_all(MYSQLI_ASSOC);
        }

        public function getCnId(){
            return $this->cnId;
        }

        public function getCnName(){
            return $this->cnName;
        }

        public function getCnNameForSystem(){
            return $this->cnNameForSystem;
        }

    }
