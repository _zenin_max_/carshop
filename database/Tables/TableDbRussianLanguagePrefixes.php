<?php

    require_once $_SERVER['DOCUMENT_ROOT'] . "/resource/path.php";
    require_once $_SERVER["DOCUMENT_ROOT"] . PathFile::$CONNECT_DB_SWEAR_WORD;

    class TableDbRussianLanguagePrefixes {

        // cn - column name
        private $cnPrefix = "prefix";
        private $cnNumberOfLetters = "number_of_letters";

        private $nameTable = "russian_language_prefixes";
        private $link;

        public function __construct(){
            $this->link = ConnectDbSwearWord::connect();
        }

        public function getMaxNumberOfLettersInRussianLanguagePrefixes(){
            $query = "SELECT MAX(`$this->cnNumberOfLetters`) AS `max_number_of_letters` FROM $this->nameTable";
            $res = $this->link->prepare($query);
            $res->execute();
            return $res->get_result()->fetch_all(MYSQLI_ASSOC)[0];
        }

        public function getPrefixByPrefix($prefix){
            $query = "SELECT * FROM `$this->nameTable` WHERE `$this->cnPrefix`=?";
            $res = $this->link->prepare($query);
            $res->bind_param("s", $prefix);
            $res->execute();
            return $res->get_result()->fetch_all(MYSQLI_ASSOC);
        }

        public function getCnPrefix(){
            return $this->cnPrefix;
        }

        public function getCnNumberOfLetters(){
            return $this->cnNumberOfLetters;
        }

    }
