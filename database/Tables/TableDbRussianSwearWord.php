<?php

    require_once $_SERVER['DOCUMENT_ROOT'] . "/resource/path.php";
    require_once $_SERVER["DOCUMENT_ROOT"] . PathFile::$CONNECT_DB_SWEAR_WORD;

    class TableDbRussianSwearWord {

        // cn - column name
        private $cnRoot= "root";
        private $cnVariationOfLetters = "variation_of_letters";

        private $nameTable = "russian_swear_word";
        private $link;

        public function __construct(){
            $this->link = ConnectDbSwearWord::connect();
        }

        public function getListSwearWordByRoot($root){
            $query = "SELECT * FROM `$this->nameTable` WHERE `$this->cnRoot` LIKE ?";
            $res = $this->link->prepare($query);
            $res->bind_param("s", $root);
            $res->execute();
            return $res->get_result()->fetch_all(MYSQLI_ASSOC);
        }

        public function getCnRoot(){
            return $this->cnRoot;
        }

        public function getCnVariationOfLetters(){
            return $this->cnVariationOfLetters;
        }

    }
