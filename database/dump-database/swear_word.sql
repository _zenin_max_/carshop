-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Хост: 127.0.0.1
-- Время создания: Ноя 26 2020 г., 23:37
-- Версия сервера: 10.4.14-MariaDB
-- Версия PHP: 7.4.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База данных: `swear_word`
--

-- --------------------------------------------------------

--
-- Структура таблицы `removing_extra_characters`
--

CREATE TABLE `removing_extra_characters` (
  `extra_character` varchar(5) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Дамп данных таблицы `removing_extra_characters`
--

INSERT INTO `removing_extra_characters` (`extra_character`) VALUES
('!'),
('#'),
('$'),
('%'),
('^'),
('&'),
('*'),
('_'),
('-'),
('+'),
(':'),
('='),
(';'),
('?'),
('\\'),
('/'),
('|');

-- --------------------------------------------------------

--
-- Структура таблицы `russian_language_prefixes`
--

CREATE TABLE `russian_language_prefixes` (
  `prefix` varchar(10) NOT NULL,
  `number_of_letters` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Дамп данных таблицы `russian_language_prefixes`
--

INSERT INTO `russian_language_prefixes` (`prefix`, `number_of_letters`) VALUES
('без', 3),
('бес', 3),
('в', 1),
('во', 2),
('воз', 3),
('вос', 3),
('возо', 4),
('вз', 2),
('вс', 2),
('вы', 2),
('до', 2),
('за', 2),
('из', 2),
('ис', 2),
('изо', 3),
('на', 2),
('наи', 3),
('недо', 4),
('над', 3),
('надо', 4),
('не', 2),
('низ', 3),
('нис', 3),
('низо', 4),
('о', 1),
('об', 2),
('обо', 3),
('обез', 4),
('обес', 4),
('от', 2),
('ото', 3),
('па', 2),
('пра', 3),
('по', 2),
('под', 3),
('подо', 4),
('пере', 4),
('пре', 3),
('пред', 4),
('предо', 5),
('при', 3),
('про', 3),
('раз', 3),
('рас', 3),
('разо', 4),
('с', 1),
('со', 2),
('су', 2),
('через', 5),
('черес', 5),
('чрез', 4),
('анти', 4),
('гипер', 5),
('дез', 3),
('дис', 3),
('кило', 4),
('контр', 5),
('макро', 5),
('микро', 5),
('мега', 4),
('мульти', 6),
('супер', 5),
('транс', 5),
('ультра', 6),
('зкстра', 6);

-- --------------------------------------------------------

--
-- Структура таблицы `russian_swear_word`
--

CREATE TABLE `russian_swear_word` (
  `root` varchar(15) NOT NULL,
  `variation_of_letters` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Дамп данных таблицы `russian_swear_word`
--

INSERT INTO `russian_swear_word` (`root`, `variation_of_letters`) VALUES
('бл*', 'я ; ю ; *'),
('еб*', 'а ; у ; е ; ё ; о ; и ; н ; к ; *'),
('п*зд*', 'я ; е ; а ; о ; ы ; и ; *'),
('сук*', 'а ; и ; *'),
('ху*', 'й ; я ; е ; ё ; *');
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
