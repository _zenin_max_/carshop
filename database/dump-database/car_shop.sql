-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Хост: 127.0.0.1
-- Время создания: Дек 29 2020 г., 01:41
-- Версия сервера: 10.4.14-MariaDB
-- Версия PHP: 7.4.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База данных: `car_shop`
--

-- --------------------------------------------------------

--
-- Структура таблицы `car-brands`
--

CREATE TABLE `car-brands` (
  `id` int(30) NOT NULL,
  `name-brand` varchar(30) NOT NULL,
  `about-brand` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Дамп данных таблицы `car-brands`
--

INSERT INTO `car-brands` (`id`, `name-brand`, `about-brand`) VALUES
(1, 'Mercedes-Benz', 'Mercedes-Benz — торговая марка и одноимённая компания — производитель легковых автомобилей выше среднего ценового сегмента, грузовых автомобилей, автобусов и других транспортных средств, входящая в состав немецкого концерна «Daimler AG». Является одним из самых узнаваемых автомобильных брендов во всём мире.'),
(2, 'BMW', 'BMW — немецкий производитель автомобилей, мотоциклов, двигателей.'),
(3, 'Lada', 'Lada — марка автомобилей, производимых АО «АвтоВАЗ». Ранее использовалась лишь для автомобилей, поставлявшихся на экспорт, а для внутреннего потребления автомобили производились под маркой «Жигули», затем — «Спутник», которое поставлялось на экспорт как «LADA Samara» и вскоре стало называться так же внутри страны.'),
(4, 'Mazda', 'Mazda — японская автомобилестроительная компания, выпускающая автомобили «Мазда».');

-- --------------------------------------------------------

--
-- Структура таблицы `cars`
--

CREATE TABLE `cars` (
  `id` int(11) NOT NULL,
  `id_brand` int(11) NOT NULL,
  `name_car` varchar(50) NOT NULL,
  `acceleration_to_100` double NOT NULL,
  `maximum_speed` double NOT NULL,
  `country_of_manufacture` varchar(60) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Дамп данных таблицы `cars`
--

INSERT INTO `cars` (`id`, `id_brand`, `name_car`, `acceleration_to_100`, `maximum_speed`, `country_of_manufacture`) VALUES
(1, 4, 'Mazda RX-7', 5.3, 250, 'Япония'),
(2, 1, 'Mercedes-Benz A200', 8.1, 224, 'Германия'),
(3, 3, 'Lada Granta', 10.9, 179, 'Россия'),
(4, 2, 'BMW X3', 8, 213, 'Германия'),
(12, 2, 'BMW X5', 1.2, 400, 'Германия');

-- --------------------------------------------------------

--
-- Структура таблицы `categories`
--

CREATE TABLE `categories` (
  `id` int(11) NOT NULL,
  `name_ru` varchar(100) NOT NULL,
  `name_for_system` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Дамп данных таблицы `categories`
--

INSERT INTO `categories` (`id`, `name_ru`, `name_for_system`) VALUES
(3, 'Все автомобили', 'PAGE_LIST_CAR'),
(1, 'Главный экран', 'PAGE_MAIN_SCREEN'),
(2, 'Проиводители автомобилей', 'PAGE_CAR_BRANDS');

-- --------------------------------------------------------

--
-- Структура таблицы `reviews_about_cars`
--

CREATE TABLE `reviews_about_cars` (
  `id` int(11) NOT NULL,
  `id_user` int(11) NOT NULL,
  `review` text NOT NULL,
  `rating` double NOT NULL,
  `date` varchar(30) NOT NULL,
  `id_car` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Структура таблицы `shops`
--

CREATE TABLE `shops` (
  `id` int(11) NOT NULL,
  `adress` varchar(256) NOT NULL,
  `coordinates` varchar(256) NOT NULL,
  `timetable` varchar(256) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Дамп данных таблицы `shops`
--

INSERT INTO `shops` (`id`, `adress`, `coordinates`, `timetable`) VALUES
(1, 'Проспект имени В.И. Ленина, 54Б', '48.725964, 44.541571', '9:00 - 21:00'),
(2, 'Улица Калинина, 13', '48.699783, 44.505701', '10:00 - 22:00'),
(3, 'Проспект имени В.И. Ленина, 65Б', '48.745199, 44.547356', '8:00 - 20:00');

-- --------------------------------------------------------

--
-- Структура таблицы `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `login` varchar(30) NOT NULL,
  `password` varchar(256) NOT NULL,
  `name` varchar(20) NOT NULL,
  `surname` varchar(20) NOT NULL,
  `mail` varchar(30) NOT NULL,
  `favorite_cars` text CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL,
  `privilege` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Дамп данных таблицы `users`
--

INSERT INTO `users` (`id`, `login`, `password`, `name`, `surname`, `mail`, `favorite_cars`, `privilege`) VALUES
(41, 'admin', '$2y$10$Pp7XY4eSdu3IgVUtM5TTROi0evUUwVkeWuOR3sK/ll0H2PP.SuOLO', 'Admin', 'Adminov', 'admin@gmail.com', ' 2 4', 1),
(42, 'max01', '$2y$10$uSIIaMENDrHy4sPI.VyvHO2C78H3EbYmex6xSd.wD5p8DQBunbNe6', 'Максим', 'Зенин', 'zenin@gmail.com', ' 1 3', 0);

--
-- Индексы сохранённых таблиц
--

--
-- Индексы таблицы `car-brands`
--
ALTER TABLE `car-brands`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `cars`
--
ALTER TABLE `cars`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `reviews_about_cars`
--
ALTER TABLE `reviews_about_cars`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `shops`
--
ALTER TABLE `shops`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT для сохранённых таблиц
--

--
-- AUTO_INCREMENT для таблицы `car-brands`
--
ALTER TABLE `car-brands`
  MODIFY `id` int(30) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=42;

--
-- AUTO_INCREMENT для таблицы `cars`
--
ALTER TABLE `cars`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=54;

--
-- AUTO_INCREMENT для таблицы `reviews_about_cars`
--
ALTER TABLE `reviews_about_cars`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=63;

--
-- AUTO_INCREMENT для таблицы `shops`
--
ALTER TABLE `shops`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT для таблицы `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=43;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
