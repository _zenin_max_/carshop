<?php
    require_once $_SERVER['DOCUMENT_ROOT'] . "/resource/path.php";
    require_once $_SERVER["DOCUMENT_ROOT"] . PathFile::$TABLE_DB_SHOPS;
    require_once $_SERVER["DOCUMENT_ROOT"] . PathFile::$CALLBACK_OPERATION;

    class Shop{

        private $id;
        private $adress;
        private $coordinates;
        private $timetable;

        private $tDbShop;

        public function __construct(){
            $this->tDbShop = new TableDbShops();
        }

        public function loadShopByIdFromDb($id){
            $shopFromDb = null;
            if ($id != null && $id != '') $shopFromDb = $this->tDbShop->getShopById($id);
            if ($shopFromDb){
                $this->id = $shopFromDb[$this->tDbShop->getCnId()];
                $this->adress = $shopFromDb[$this->tDbShop->getCnAdress()];
                $this->coordinates = $shopFromDb[$this->tDbShop->getCnCoordinates()];
                $this->timetable = $shopFromDb[$this->tDbShop->getCnTimetable()];
            }
        }

        public function getId(){
            return $this->id;
        }
        public function setId($id){
            $this->id = $id;
        }

        public function getAdress(){
            return $this->adress;
        }
        public function setAdress($adress){
            $this->adress = $adress;
        }

        public function getCoordinates(){
            return $this->coordinates;
        }
        public function setCoordinates($coordinates){
            $this->coordinates = $coordinates;
        }

        public function getTimetable(){
            return $this->timetable;
        }
        public function setTimetable($timetable){
            $this->timetable = $timetable;
        }

    }
