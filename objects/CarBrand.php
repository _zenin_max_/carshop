<?php
    require_once $_SERVER['DOCUMENT_ROOT'] . "/resource/path.php";
    require_once $_SERVER['DOCUMENT_ROOT'] . PathFile::$TABLE_DB_CAR_BRANDS;
    require_once $_SERVER['DOCUMENT_ROOT'] . PathFile::$TABLE_DB_CARS;
    require_once $_SERVER['DOCUMENT_ROOT'] . PathFile::$CALLBACK_OPERATION;

    class CarBrand{

        private $id;
        private $nameBrand;
        private $aboutBrand;
        private $pathLogo;

        private $tDbCarBrand;
        private $tDbCars;

        public function __construct($id = null, $nameBrand = null, $aboutBrand = null){
            $this->tDbCarBrand = new TableDbCarBrands();
            $this->tDbCars = new TableDbCars();
            if ($id != null){
                $this->id = $id;
            }
            if ($nameBrand != null) $this->nameBrand = $nameBrand;
            if ($aboutBrand != null) $this->aboutBrand = $aboutBrand;
        }

        private function makePathLogo(){
            $path = PathTools::makePathToProjectRoot() . 'img/cars/'
                . str_replace(' ','-', mb_strtolower($this->nameBrand))
                . '/logo-brand/logo.png';

            if (is_readable($path)) $this->pathLogo = $path;
            else $this->pathLogo = PathTools::makePathToProjectRoot() . 'img/no_photo.png';
        }

        public function loadCarBrandFromDbById(){
            $carBrandFromDB = $this->tDbCarBrand->getCarBrandById($this->id);
            $this->nameBrand = $carBrandFromDB[$this->tDbCarBrand->getCnNameBrand()];
            $this->aboutBrand = $carBrandFromDB[$this->tDbCarBrand->getCnAboutBrand()];
        }

        public function insertCarBrandInDB(CallbackOperation $callbackOperation){
            $this->tDbCarBrand->insertCarBrand($this->nameBrand, $this->aboutBrand, $callbackOperation);
        }

        public function updateCarBrandInDB(CallbackOperation $callbackOperation){
            $this->tDbCarBrand->updateCarBrand($this->id, $this->nameBrand, $this->aboutBrand, $callbackOperation);
        }

        public function deleteCarBrandInDb(CallbackOperation $callbackOperation){
            $this->tDbCars->deleteAllCarsByIdCarBrand($this->id);
            $this->tDbCarBrand->deleteCarBrands($this->id, $callbackOperation);
        }

        public function getPathLogo(){
            $this->makePathLogo();
            return $this->pathLogo;
        }

        public function getId(){
            return $this->id;
        }
        public function setId($id){
            $this->id = $id;
        }

        public function getNameBrand(){
            return $this->nameBrand;
        }
        public function setNameBrand($nameBrand){
            $this->nameBrand = $nameBrand;
        }

        public function getAboutBrand(){
            return $this->aboutBrand;
        }
        public function setAboutBrand($aboutBrand){
            $this->aboutBrand = $aboutBrand;
        }

    }