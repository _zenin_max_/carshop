<?php
    require_once $_SERVER['DOCUMENT_ROOT'] . "/resource/path.php";
    require_once $_SERVER['DOCUMENT_ROOT'] . PathFile::$OBJECT_SHOP;
    require_once $_SERVER['DOCUMENT_ROOT'] . PathFile::$TABLE_DB_SHOPS;

    class ListShops{

        private $tDbShops;

        public function __construct(){
            $this->tDbShops = new TableDbShops();
        }

        public function getAllShops(){
            $listShops = array();
            $listShopsFromDb = $this->tDbShops->getAllShops();
            foreach ($listShopsFromDb as $shopFromDb){
                array_push($listShops, $this->makeShop($shopFromDb));
            }
            return $listShops;
        }

        private function makeShop($shopFromDb){
            $shop = new Shop();
            $shop->setId($shopFromDb[$this->tDbShops->getCnId()]);
            $shop->setAdress($shopFromDb[$this->tDbShops->getCnAdress()]);
            $shop->setCoordinates($shopFromDb[$this->tDbShops->getCnCoordinates()]);
            $shop->setTimetable($shopFromDb[$this->tDbShops->getCnTimetable()]);
            return $shop;
        }

    }
