<?php
    require_once $_SERVER['DOCUMENT_ROOT'] . "/resource/path.php";
    require_once $_SERVER['DOCUMENT_ROOT'] . PathFile::$OBJECT_CAR;
    require_once $_SERVER['DOCUMENT_ROOT'] . PathFile::$TABLE_DB_CARS;

    class ListCars{

        private $tDbCars;

        public function __construct(){
            $this->tDbCars = new TableDbCars();
        }

        public function getAllCars(){
            $listCars = array();
            $listCarsFromDb = $this->tDbCars->getListAllCars();
            foreach ($listCarsFromDb as $carFromDb){
                array_push($listCars, $this->makeCar($carFromDb));
            }
            return $listCars;
        }

        public function getListCarsByIdBrand($idBrand){
            $listCars = array();
            $listCarsFromDb = $this->tDbCars->getListCarsByIdBrand($idBrand);
            foreach ($listCarsFromDb as $carFromDb){
                array_push($listCars, $this->makeCar($carFromDb));
            }
            return $listCars;
        }

        public function searchCarsByWord($word){
            $listCars = array();
            $listCarsFromDb = $this->tDbCars->searchCarsByWord($word);
            foreach ($listCarsFromDb as $carFromDb){
                array_push($listCars, $this->makeCar($carFromDb));
            }
            return $listCars;
        }

        private function makeCar($carFromDb){
            $car = new Car($carFromDb[$this->tDbCars->getCnId()], $carFromDb[$this->tDbCars->getCnNameCar()],
                $carFromDb[$this->tDbCars->getCnIdBrand()], $carFromDb[$this->tDbCars->getCnAccelerationTo100()],
                $carFromDb[$this->tDbCars->getCnMaximumSpeed()], $carFromDb[$this->tDbCars->getCnCountryOfManufacture()]
            );
            return $car;
        }



    }
