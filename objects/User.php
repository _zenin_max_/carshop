<?php
    require_once $_SERVER["DOCUMENT_ROOT"] . '\resource\path.php';
    require_once $_SERVER["DOCUMENT_ROOT"] . PathFile::$TABLE_DB_USERS;
    require_once $_SERVER["DOCUMENT_ROOT"] . PathFile::$OBJECT_PASSWORD;
    require_once $_SERVER["DOCUMENT_ROOT"] . PathFile::$CALLBACK_OPERATION;
    require_once $_SERVER["DOCUMENT_ROOT"] . PathFile::$OBJECT_CAR;
    require_once $_SERVER["DOCUMENT_ROOT"] . PathFile::$STRING;

    class User {

        private $id;
        private $login;
        private $passwordHash;
        private $name;
        private $surname;
        private $mail;
        private $favoriteCars;
        private $privilege;

        private $tDbUsers;

        public function __construct($id = null){
            $this->tDbUsers = new TableDbUsers();
            if ($id !== null)
                $this->loadUserByIdFromDB($id);
        }

        public function getId(){
            return $this->id;
        }
        public function setId($id){
            $this->id = $id;
        }

        public function getLogin(){
            return $this->login;
        }
        public function setLogin($login, CallbackOperation $callbackOperation){
            if (strlen($login) < 4){
                ($callbackOperation->onFail)(Status::$FAIL, 'Логин должен содержать минимум 4 символа.');
            } elseif (strlen($login) >= 20){
                ($callbackOperation->onFail)(Status::$FAIL, 'Логин должен содержать меньше 20 символов.');
            } elseif (!preg_match("/^[a-zA-Z\d]*$/", $login)){
                ($callbackOperation->onFail)(Status::$FAIL, 'В логине должны быть только симаолы A-Z, a-z, 0-9.');
            } elseif ($this->tDbUsers->getUserByLogin($login)){
                ($callbackOperation->onFail)(Status::$FAIL, 'Пользователь с таким логином уже существует.');
            } else {
                $this->login = $login;
            }
        }

        public function getPasswordHash(){
            return $this->passwordHash;
        }
        public function setPassword($password, $repeatPassword, CallbackOperation $callbackOperation){
            $pw = new Password();
            $pw->setPassword($password, $repeatPassword, $callbackOperation);
            $this->passwordHash = $pw->getPasswordHash();
        }

        public function getName(){
            return $this->name;
        }
        public function setName($name, CallbackOperation $callbackOperation){
            if (strlen($name) < 1 ){
                ($callbackOperation->onFail)(Status::$FAIL, 'Имя должно содержать больше одного символа');
            } elseif (strlen($name) >= 20){
                ($callbackOperation->onFail)(Status::$FAIL, 'Имя должно содержать меньше 20 символов');
            } else {
                $this->name = $name;
            }
        }

        public function getSurname(){
            return $this->surname;
        }
        public function setSurname($surname, CallbackOperation $callbackOperation){
            if (strlen($surname) < 3 ){
                ($callbackOperation->onFail)(Status::$FAIL, 'Фамилия должна содержать больше 3-ёх символов.');
            } elseif (strlen($surname) >= 20){
                ($callbackOperation->onFail)(Status::$FAIL, 'Фамилия должна содержать меньше 20 символов.');
            } else {
                $this->surname = $surname;
            }
        }

        public function getMail(){
            return $this->mail;
        }
        public function setMail($mail){
            $this->mail = $mail;
        }

        public function getFavoriteCars(){
            return $this->favoriteCars;
        }
        public function setFavoriteCars($favoriteCars){
            $this->favoriteCars = $favoriteCars;
        }

        public function getPrivilege(){
            return $this->privilege;
        }

        private function checkForRemoteCar($favoriteCars){
            $isWasHere = false;
            foreach (explode(' ', $favoriteCars) as $idCar){
                $car = new Car($idCar);
                $car->loadCarByIdFromDb();
                if (!$car->getNameCar()){
                    $favoriteCars = str_replace( $idCar,'', $favoriteCars);
                    $favoriteCars = str_replace( '  ',' ', $favoriteCars);
                    $isWasHere = true;
                }
            }
            if ($isWasHere){
                $this->favoriteCars = $favoriteCars;
                $this->updateUserInDb();
            }
            return $favoriteCars;
        }

        public function insertUserInDB(CallbackOperation $callbackOperation){
            $this->tDbUsers->insertNewUser($this->login, $this->passwordHash, $this->name, $this->surname, $this->mail,
                $callbackOperation);
        }

        private function loadUserByIdFromDB($id){
            $user = array();
            if ($id != '' || $id != null)
                $user = $this->tDbUsers->getUserById($id);
            if ($user){
                $this->id = $user[$this->tDbUsers->getCnId()];
                $this->login = $user[$this->tDbUsers->getCnLogin()];
                $this->name = $user[$this->tDbUsers->getCnName()];
                $this->surname = $user[$this->tDbUsers->getCnSurname()];
                $this->mail = $user[$this->tDbUsers->getCnMail()];
                $this->privilege = $user[$this->tDbUsers->getCnPrivilege()];
                $this->passwordHash = $user[$this->tDbUsers->getCnPassword()];
                $this->favoriteCars = $this->checkForRemoteCar($user[$this->tDbUsers->getCnFavoriteCars()]);
            }
        }

        public function loadUserByLoginFromDB($login){
            $user = array();
            if ($login != '' || $login != null)
                $user = $this->tDbUsers->getUserByLogin($login)[0];
            if ($user){
                $this->id = $user[$this->tDbUsers->getCnId()];
                $this->login = $user[$this->tDbUsers->getCnLogin()];
                $this->name = $user[$this->tDbUsers->getCnName()];
                $this->surname = $user[$this->tDbUsers->getCnSurname()];
                $this->mail = $user[$this->tDbUsers->getCnMail()];
                $this->privilege = $user[$this->tDbUsers->getCnPrivilege()];
                $this->passwordHash = $user[$this->tDbUsers->getCnPassword()];
                $this->favoriteCars = $this->checkForRemoteCar($user[$this->tDbUsers->getCnFavoriteCars()]);
            }
            return $user;
        }

        public function updateUserInDb(){
            $this->tDbUsers->updateUser($this->id, $this->login, $this->name, $this->surname, $this->mail, $this->favoriteCars);
        }
    }
?>
