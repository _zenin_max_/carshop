class ButtonFavorite {

    _idCar;
    _idUser;
    _idElementImg;
    _isFavorite;
    _srcImgFavorite = "img/favorite.png";
    _srcImgNoFavorite = "img/no_favorite.png";
    _elementImg;

    constructor(idCar, idUser, idElementImg, isFavorite) {
        this._idUser = idUser;
        this._idCar = idCar;
        this._idElementImg = idElementImg;
        this._isFavorite = isFavorite;
        this._elementImg = document.getElementById(idElementImg);
        this.setOnClick();
        this.setImgButton();
    }

    setOnClick(){
        const v = this;
        this._elementImg.onclick = function () {
            v.changeIsFavorite();
        }
    }

    changeIsFavorite(){
        this._isFavorite = !this._isFavorite;
        this.setImgButton();
        this.updateInBD()
    }

    setImgButton(){
        this._elementImg.src = (this._isFavorite) ? this._srcImgFavorite : this._srcImgNoFavorite;
    }

    updateInBD(){
        location.href = "methods/method-favorite-car.php?id_car=" + this._idCar + "&set_favorite=" + this._isFavorite ;
    }

}