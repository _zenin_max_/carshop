<?php
    require_once $_SERVER['DOCUMENT_ROOT'] . "/resource/path.php";
    require_once $_SERVER["DOCUMENT_ROOT"] . PathFile::$OBJECT_CAR_BRAND;
    require_once $_SERVER["DOCUMENT_ROOT"] . PathFile::$TABLE_DB_CARS;
    require_once $_SERVER["DOCUMENT_ROOT"] . PathFile::$CALLBACK_OPERATION;

    class Car {
        private $id;
        private $nameCar;
        private $listPhotoPath;
        private $carBrand;
        private $accelerationTo100;
        private $maximumSpeed;
        private $listCharacteristicWithValue;
        private $countryOfManufacture;

        private $tDbCars;

        static public $listNameCharacteristic = array(
                array('nameSystem' => 'accelerationTo100', 'ru' => 'Разгон до 100км/ч'),
                array('nameSystem' => 'maximumSpeed', 'ru' => 'Максимальная скорость км/ч'),
                array('nameSystem' => 'countryOfManufacture', 'ru' => 'Страна производитель'),
        );

        public function __construct($id = null, $nameCar = null, $idCarBrand = null, $accelerationTo100 = null, $maximumSpeed = null,
                                    $countryOfManufacture = null){

            $this->tDbCars = new TableDbCars();

            if ($id != null) $this->id = $id;
            if ($idCarBrand != null) {
                $this->carBrand = new CarBrand($idCarBrand);
                $this->carBrand->loadCarBrandFromDbById();
            }
            if ($nameCar != null) $this->nameCar = $nameCar;
            if ($accelerationTo100 != null) $this->accelerationTo100 = $accelerationTo100;
            if ($maximumSpeed != null) $this->maximumSpeed = $maximumSpeed;
            if ($countryOfManufacture != null) $this->countryOfManufacture = $countryOfManufacture;
        }

        public function loadCarByIdFromDb(){
            $carFromDb = null;
            if ($this->id != null && $this->id != '')  $carFromDb = $this->tDbCars->getCarById($this->id);
            if ($carFromDb){

                $this->__construct($carFromDb[$this->tDbCars->getCnId()], $carFromDb[$this->tDbCars->getCnNameCar()],
                    $carFromDb[$this->tDbCars->getCnIdBrand()], $carFromDb[$this->tDbCars->getCnAccelerationTo100()],
                    $carFromDb[$this->tDbCars->getCnMaximumSpeed()], $carFromDb[$this->tDbCars->getCnCountryOfManufacture()]
                );

            }
        }

        private function makeListCharacteristicWithValue(){
            $this->listCharacteristicWithValue = array(
                array('characteristic' => 'Разгон до 100км/ч', 'value' => $this->accelerationTo100),
                array('characteristic' => 'Максимальная скорость км/ч', 'value' => $this->maximumSpeed),
                array('characteristic' => 'Страна производитель', 'value' => $this->countryOfManufacture),
            );
        }

        private function makeListPhotoPath(){
            $this->listPhotoPath = array();

            $pathDir = PathTools::makePathToProjectRoot() . 'img/cars/' . str_replace(' ','-', mb_strtolower($this->carBrand->getNameBrand())) . '/'
                . str_replace(' ','-', mb_strtolower($this->nameCar));

            if (is_dir($pathDir)) $nPhoto = iterator_count(new FilesystemIterator($pathDir, FilesystemIterator::SKIP_DOTS));
            else {
                array_push($this->listPhotoPath, PathTools::makePathToProjectRoot() . 'img/no_photo.png');
                return;
            }

            for($i = 0; $i < $nPhoto; $i++){
                $pathFile = $pathDir . '/' . str_replace(' ','-', mb_strtolower($this->nameCar)) . '-' . $i . '.jpg';
                if (is_readable($pathFile)) array_push($this->listPhotoPath, $pathFile);
            }
        }

        public function insertCarInDB(CallbackOperation $callbackOperation){
            $this->tDbCars->insertCar($this->carBrand->getId(), $this->nameCar, $this->accelerationTo100,
                $this->maximumSpeed, $this->countryOfManufacture, $callbackOperation);
        }

        public function updateCarInDB(CallbackOperation $callbackOperation){
            $this->tDbCars->updateCar($this->id, $this->carBrand->getId(), $this->nameCar, $this->accelerationTo100,
                $this->maximumSpeed, $this->countryOfManufacture, $callbackOperation);
        }

        public function deleteCarInDB(CallbackOperation $callbackOperation){
            $this->tDbCars->deleteCar($this->id, $callbackOperation);
        }


        public function getId(){
            return $this->id;
        }
        public function setId($id){
            $this->id = $id;
        }

        public function getNameCar(){
            return $this->nameCar;
        }
        public function setNameCar($nameCar){
            $this->nameCar = $nameCar;
        }

        public function getListPhotoPath(){
            $this->makeListPhotoPath();
            return $this->listPhotoPath;
        }

        public function getCarBrand(){
            return $this->carBrand;
        }
        public function setCarBrand($carBrand){
            $this->carBrand = $carBrand;
        }

        public function getAccelerationTo100(){
            return $this->accelerationTo100;
        }
        public function setAccelerationTo100($accelerationTo100){
            $this->accelerationTo100 = $accelerationTo100;
        }

        public function getMaximumSpeed(){
            return $this->maximumSpeed;
        }
        public function setMaximumSpeed($maximumSpeed){
            $this->maximumSpeed = $maximumSpeed;
        }

        public function getListCharacteristicWithValue(){
            $this->makeListCharacteristicWithValue();
            return $this->listCharacteristicWithValue;
        }

        public function getCountryOfManufacture(){
            return $this->countryOfManufacture;
        }
        public function setCountryOfManufacture($countryOfManufacture){
            $this->countryOfManufacture = $countryOfManufacture;
        }

    }
