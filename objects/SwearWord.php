<?php
    require_once $_SERVER['DOCUMENT_ROOT'] . "/resource/path.php";
    require_once $_SERVER['DOCUMENT_ROOT'] . PathFile::$TABLE_DB_RUSSIAN_LANGUAGE_PREFIXES;
    require_once $_SERVER['DOCUMENT_ROOT'] . PathFile::$TABLE_DB_RUSSIAN_SWEAR_WORD;
    require_once $_SERVER['DOCUMENT_ROOT'] . PathFile::$TABLE_DB_REMOVING_EXTRA_CHARACTERS;

    class SwearWord{

        private $text;
        private $tDbRemovingExtraCharacters;
        private $tDbRussianSwearWord;
        private $tDbRussianLanguagePrefixes;

        public function __construct($text = null){
            $this->tDbRemovingExtraCharacters = new TableDbRemovingExtraCharacters();
            $this->tDbRussianSwearWord = new TableDbRussianSwearWord();
            $this->tDbRussianLanguagePrefixes = new TableDbRussianLanguagePrefixes();

            if ($text !== null)
                $this->text = $text;
        }

        public function getText(){
            return $this->text;
        }

        public function setText($text){
            $this->text = $text;
        }

        public function removeSwearWordsFromText(){
            // разделяю текст на слова
            $words = explode(" ", $this->text);

            $this->text = '';
            // собираю тот же текст только с отфильтрованными лт мата словами
            foreach($words as $word){
                // если данное слово матно то заменить его на символ '#'
                if($this->checkWordForAnSwearWord($word))
                    $word = $this->changeWordToCharacter($word, '#');
                $this->text .= $word . ' ';
            }
        }

        // заменяет слово на символы
        private function changeWordToCharacter($word, $symbol){
            $new_word = '';
            for ($i = 0; $i < mb_strlen($word); $i++)
                $new_word .= $symbol;
            return $new_word;
        }

        //проаеряет входное слово на мат
        private function checkWordForAnSwearWord($word){
            // переводит слово к нижнему регистру
            $word = mb_strtolower($word);

            // удаляет из слова лишние символы
            // Например: б%л?я#т*??ь
            $word = $this->removingExtraCharacters($word);

            $isSwearWord = 0;
            while(1){
                // проверят корень слова на мат
                if ($this->checkRootForSwearWord($word) == 1){
                    $isSwearWord = 1;
                    break;
                } else {

                    // удаляет из слова приставку
                    $newWordWithoutPrefix = $this->deleteWordPrefix($word);

                    // если слово изменилось(т.е. удалилась приставка из слова),
                    // значит нужно проверить корень слова на мат уже без приставки.
                    // иначе если слово не нашлось то приставка не была найдена,
                    // а раз корень слова не содержит мат, и не найдена приставка,
                    // значит анализ слова можно остановить и сделать вывод что это не матное слово
                    if ($word != $newWordWithoutPrefix){
                        $word = $newWordWithoutPrefix;
                    } else {
                        $isSwearWord = 0;
                        break;
                    }
                }
            }
            return $isSwearWord;
        }

        // Если в слове есть приставка то удаляет её
        private function deleteWordPrefix($word){
            // в слове ищется сначала самая большая приставка(по кол-ву букв),
            // если искать сначала от самых маленьких приставок,
            // то например не найдется приставка 'под',
            // так как есть приставка 'по'

            // число букв в самой большой приставке
            $maxNumberOfLettersInPrefix = $this->tDbRussianLanguagePrefixes->getMaxNumberOfLettersInRussianLanguagePrefixes()['max_number_of_letters'];

            // если число букв в самой большой приставке больше чем букв в самом слове
            $i = (mb_strlen($word) < $maxNumberOfLettersInPrefix) ? mb_strlen($word) - 1 : $maxNumberOfLettersInPrefix - 1;


            for (; $i >= 0; $i--){
                // выделяет приставку
                $prefix = mb_substr($word, 0, $i + 1);

                // если приставка найдена, то она удаляется из слова
                if($this->tDbRussianLanguagePrefixes->getPrefixByPrefix($prefix)){
                    $word = mb_substr($word, $i + 1, mb_strlen($word));
                    break;
                }
            }
            return $word;
        }

        // проверка корня слова на мат.
        // Подразумевается что слово подаётся уже без приставки, и проверка корня начинается с первой буквы слова
        private function checkRootForSwearWord($word){
            $isSwearWord = 0;
            for ($i = 0; $i < mb_strlen($word); $i++){

                // выделяет корень слова
                $root = mb_substr($word, 0, $i + 1);

                // выводит список матных слов начинающихся на выделенный корень
                $listSwearWord = $this->tDbRussianSwearWord->getListSwearWordByRoot($root . '%');

                // проверка на нул поинтер ексепшен
                if (array_key_exists (0, $listSwearWord))
                    $listSwearWord = $listSwearWord[0];

                // если есть матные слова начинающиеся на выделенный корень слова
                if($listSwearWord){

                    // Проверка на '*' следующую букву. Например 'ху*' вместо звёздочки могут быть буквы (й ; я ; е ; ё )
                    // и если в корне слова нашлось 'ху' а потом идет одна из букв, то слово будет считаться матным.
                    if (mb_substr($listSwearWord['root'], $i + 1, 1) == '*' ){
                        $listVariationOfLetters = explode(' ; ', $listSwearWord['variation_of_letters']);
                        $isFindLatter = 0;

                        // перебор списка букв которые могут быть
                        foreach ($listVariationOfLetters as $letter){
                            if (mb_substr($word, $i + 1, 1) == $letter){
                                $isFindLatter = 1;
                                $word = mb_substr($word, 0, $i + 1) . '*' .  mb_substr($word, $i + 2, mb_strlen($word));
                                break;
                            }
                        }
                        if ($isFindLatter == 0){
                            break;
                        }

                    }

                    // если корень слова полностью совпадает с матным словом, то слово считается матным
                    if ($i ==  mb_strlen($listSwearWord['root']) - 2){
                        $isSwearWord = 1;
                        break;
                    }

                    // иначе если нет мантных слов начинающихся на выделенный корень, то считается что слово не матное
                } else {
                    $isSwearWord = 0;
                    break;
                }
            }
            return $isSwearWord;
        }

        // удаляет из слова лишние символы
        // Например: б%л?я#т*??ь
        private function removingExtraCharacters($word){
            while (1){
                for($i = 0; $i < mb_strlen($word); $i++){
                    $symbol = mb_substr($word, $i, 1);
                    if ($this->tDbRemovingExtraCharacters->getSymbolBySymbol($symbol)){
                        $word = str_replace($symbol,'',$word);
                        break;
                    }
                }
                if ($i == mb_strlen($word))
                    break;
            }
            return $word;
        }
    }