<?php
    require_once $_SERVER['DOCUMENT_ROOT'] . "/resource/path.php";
    require_once $_SERVER['DOCUMENT_ROOT'] . PathFile::$OBJECT_CAR_BRAND;
    require_once $_SERVER['DOCUMENT_ROOT'] . PathFile::$TABLE_DB_CAR_BRANDS;

    class ListCarBrands{

        private $tDbCarBrands;

        public function __construct(){
            $this->tDbCarBrands = new TableDbCarBrands();
        }

        public function getListAllCarBrands(){
            $listCarBrands = array();
            $listCarBrandsFromDb = $this->tDbCarBrands->getListAllCarBrands();
            foreach ($listCarBrandsFromDb as $carBrandsFromDb){
                $carBrand = new CarBrand($carBrandsFromDb[$this->tDbCarBrands->getCnId()],
                    $carBrandsFromDb[$this->tDbCarBrands->getCnNameBrand()],
                    $carBrandsFromDb[$this->tDbCarBrands->getCnAboutBrand()]);
                array_push($listCarBrands, $carBrand);
            }
            return $listCarBrands;
        }

        

    }