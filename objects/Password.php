<?php
    require_once $_SERVER['DOCUMENT_ROOT'] . "/resource/path.php";
    require_once $_SERVER["DOCUMENT_ROOT"] . PathFile::$CALLBACK_OPERATION;
    require_once $_SERVER["DOCUMENT_ROOT"] . PathFile::$STRING;

    class Password{

        private $password;
        private $passwordHash;

        private function passwordSecurityCheck($password, $repeatPassword){

            if ($password !== $repeatPassword){
                return 'пароли не совпадают';
            } elseif (strlen($password) < 8){
                return 'Пароль должен содержать минимум 8 символов.';
            } elseif(!preg_match("/^[a-zA-Z\d]*$/", $password)){
                return 'В пароле должны быть только симаолы A-Z, a-z, 0-9.';
            } else {
                return Status::$SUCCESS;
            }
        }

        private function hashingPassword($password){
            return password_hash($password, PASSWORD_DEFAULT);
        }

        public function getPassword(){
            return $this->password;
        }

        public function setPassword($password, $repeatPassword, CallbackOperation $callbackOperation){
            $passwordSecurity = $this->passwordSecurityCheck($password, $repeatPassword);
            if ($passwordSecurity == Status::$SUCCESS){
                $this->password = $password;
            } else {
                ($callbackOperation->onFail)(Status::$WRONG_FORMAT, $passwordSecurity);
            }
        }

        public function getPasswordHash(){
            if (!$this->passwordHash && $this->password){
                $this->passwordHash = $this->hashingPassword($this->password);
            }
            return $this->passwordHash;
        }

        public function setPasswordHash($passwordHash){
            $this->passwordHash = $passwordHash;
        }

        public function isPasswordsHashSame($password2){
            return password_verify($password2, $this->passwordHash);
        }
    }