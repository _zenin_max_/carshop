<?php

    class CallbackOperation{
        public $onSuccess;
        public $onFail;

        public function __construct($onSuccess, $onFail){
            $onSuccess ? $this->onSuccess = $onSuccess : $this->onSuccess = function (){};;
            $onFail ? $this->onFail = $onFail : $this->onFail = function (){};
        }
    }
