<?php  
    session_start();
    require $_SERVER['DOCUMENT_ROOT'] . "/resource/path.php";
    require $_SERVER['DOCUMENT_ROOT'] . PathFile::$TABLE_DB_USERS;
    require $_SERVER['DOCUMENT_ROOT'] . PathFile::$OBJECT_USER;
    require $_SERVER['DOCUMENT_ROOT'] . PathFile::$OBJECT_LIST_SHOP;

    $page = PathPage::$PAGE_MAIN_SCREEN;
    $user = new User( (array_key_exists ( 'userId' , $_SESSION )) ? $_SESSION['userId'] : null);
?>

<!DOCTYPE html>
<html lang="ru">
<head>  
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Главный экран</title>

  <?php require $_SERVER['DOCUMENT_ROOT'] . PathFile::$BLOCK_BOOTSTRAP; ?>

  <link rel="stylesheet" href="<?php echo PathTools::makePathToProjectRoot() . PathFile::$CSS_CARD?>">
  <link rel="stylesheet" href="<?php echo PathTools::makePathToProjectRoot() . PathFile::$CSS_MAIN?>">

</head>
<body>
    
  <div>
        
    <?php require $_SERVER['DOCUMENT_ROOT'] . PathFile::$BLOCK_HEADER ?>

    <div class="container">
        <?php
            $itemsMenu = array(
                array('img' => PathTools::makePathToProjectRoot() . 'img/cars_1.png', 'title' => 'Автомобили со всего мира'),
                array('img' => PathTools::makePathToProjectRoot() . 'img/reliability.jpg', 'title' => 'Надёжность')
            );

            for ($index = 0; $index < count($itemsMenu); $index++){
                $imgItem = $itemsMenu[$index]['img'];
                $titleItem = $itemsMenu[$index]['title'];
                require $_SERVER['DOCUMENT_ROOT'] . PathFile::$ITEM_MAIN_MENU;
            }
        ?>

<!--        <div id="map" style="width: 100%; height:500px"></div>-->
        <?php
            $listShops = new ListShops();
            $listAllShops = $listShops->getAllShops();
        ?>


        <div class="card-grey d-flex flex-wrap justify-content-center  mb-4 " style="width: 100%;" >
            <div style="width: 45%; min-width: 20rem">
                <h2 class="m-2">Адреса магазинов</h2>
                <?php   foreach ($listAllShops as $shop){ ?>
                            <div class="card-in-card p-2 m-2" style="width: auto">
                                <p style="font-weight: bold">
                                    <?php echo ' Адрес: ' . $shop->getAdress() . ' <br> Время работы: ' . $shop->getTimetable()?>
                                </p>
                            </div>
                <?php   } ?>
            </div>
            <div id="map" class="m-2" style="width: 45%; height:400px; min-width: 20rem"></div>
        </div>


    </div>

  </div>

</body>
</html>

<script src="https://api-maps.yandex.ru/2.1/?lang=ru-RU" type="text/javascript"></script>
<script type="text/javascript">
    ymaps.ready(init);
    function init() {
        var myMap = new ymaps.Map("map", {
            center: '<?php echo $listAllShops[0]->getCoordinates();?>',
            zoom: 16
        }, {
            searchControlProvider: 'yandex#search'
        });
        var myCollection = new ymaps.GeoObjectCollection();
        <?php   foreach ($listAllShops as $shop){ ?>
        var myPlacemark = new ymaps.Placemark([
            <?php echo $shop->getCoordinates()?>
        ], {
            balloonContent: '<?php echo ' Адрес: ' . $shop->getAdress() . ' <br> Время работы: ' . $shop->getTimetable()?>'
        }, {
            preset: 'islands#icon',
            iconColor: '#0000ff'
        });
        myCollection.add(myPlacemark);
        <?php   }?>
        myMap.geoObjects.add(myCollection);
        myMap.setBounds(myCollection.getBounds(),{checkZoomRange:true, zoomMargin:9});
    }
</script>