<?php
    session_start();
    require $_SERVER['DOCUMENT_ROOT'] . "/resource/path.php";
    $page = PathPage::$PAGE_SUCCESSFUL_REGISTRATION;
?>

<!doctype html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Регистрация</title>

    <?php require $_SERVER['DOCUMENT_ROOT'] . PathFile::$BLOCK_BOOTSTRAP; ?>

    <link rel="stylesheet" href="<?php echo PathTools::makePathToProjectRoot() . PathFile::$CSS_CARD?>">
    <link rel="stylesheet" href="<?php echo PathTools::makePathToProjectRoot() . PathFile::$CSS_MAIN?>">

</head>
<body>

    <?php require $_SERVER['DOCUMENT_ROOT'] . PathFile::$BLOCK_HEADER; ?>

    <div class="color-text-1" style="width: 100%; text-align: center; font-weight: bolder; font-size: 2em; margin-top: 15%;">
        Регистрация прошла успешно
    </div>

</body>
</html>