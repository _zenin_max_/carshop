<?php
    session_start();

    require $_SERVER['DOCUMENT_ROOT'] . "/resource/path.php";
    require $_SERVER["DOCUMENT_ROOT"] . PathFile::$TABLE_DB_USERS;
    require $_SERVER["DOCUMENT_ROOT"] . PathFile::$OBJECT_PASSWORD;
    require $_SERVER["DOCUMENT_ROOT"] . PathFile::$OBJECT_USER;

    $user = new User();
    $user->loadUserByLoginFromDB($_POST['login']);
    $password = new Password();
    $password->setPasswordHash($user->getPasswordHash());

    if ($user->getId() && $password->isPasswordsHashSame($_POST['password']) && $user->getLogin() === $_POST['login']){
        $_SESSION['userId'] = $user->getId();
        header("Location: " . PathTools::makePathToProjectRoot() . PathPage::$PAGE_PROFILE);
    } elseif(!$user->getId() || $user->getLogin() !== $_POST['login']){
        header("Location: " . PathTools::makePathToProjectRoot() . PathPage::$PAGE_SIGN_IN . '?wrong_login=true');
    } elseif (!$password->isPasswordsHashSame($_POST['password'])){
        header("Location: " .  PathTools::makePathToProjectRoot() . PathPage::$PAGE_SIGN_IN . '?wrong_password=true');
    }
