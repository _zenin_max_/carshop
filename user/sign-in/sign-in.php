<?php 
    session_start();
    require $_SERVER['DOCUMENT_ROOT'] . "/resource/path.php";
    require $_SERVER['DOCUMENT_ROOT'] . PathFile::$TABLE_DB_USERS;

    $page = PathPage::$PAGE_SIGN_IN;
?>
  
<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Войти</title>

    <?php require $_SERVER['DOCUMENT_ROOT'] . PathFile::$BLOCK_BOOTSTRAP; ?>

    <link rel="stylesheet" href="<?php echo PathTools::makePathToProjectRoot() . PathFile::$CSS_CARD?>">
    <link rel="stylesheet" href="<?php echo PathTools::makePathToProjectRoot() . PathFile::$CSS_MAIN?>">

</head>
<body >
  
    <?php require $_SERVER['DOCUMENT_ROOT'] . PathFile::$BLOCK_HEADER; ?>

    <div class="card card-grey container-fluid  " style="width: 25rem;">
        <img class="card-img-top mt-3 mb-2" src="../../img/logo-sign-in.svg" width="72" height="72" alt="Sign in">
        <div class="card-body container-fluid d-flex flex-wrap justify-content-center">

            <form action="<?php echo PathTools::makePathToProjectRoot() . PathFile::$METHOD_SIGN_IN?>" id="sign_in" method="POST" style="width: 100%;" class="mb-2" >

                <div >
                    <label for="inputLogin" class="sr-only" >Логин</label>
                    <input type="text" id="inputLogin" style="width: 100%;" name="login" class="form-control input-login-password" placeholder="Логин"  required autofocus>
                </div>

                <?php if (array_key_exists('wrong_login', $_GET) and $_GET['wrong_login'] = true) { ?>
                    <p style="color: red; font-weight: normal; width: 100%; text-align: center;">Неверный логин</p>
                <?php } ?>

                <div class="mt-1">
                    <label for="inputPassword" class="sr-only">Пароль</label>
                    <input type="password" id="inputPassword" style="width: 100%;" name="password" class="form-control input-email-password" placeholder="Пароль" required>
                </div>

                <?php if (array_key_exists('wrong_password', $_GET) and $_GET['wrong_password'] = true) { ?>
                    <p style="color: red; font-weight: normal; width: 100%; text-align: center;">Неверный пароль</p>
                <?php } ?>

            </form>

            <button style="width: 45%;" class="btn btn-primary m-1" onclick="document.getElementById('sign_in').submit(); return false;">
                Войти
            </button>

            <button style="width: 45%;" class="btn btn-primary m-1" onclick="window.location.href='<?php echo PathTools::makePathToProjectRoot() . PathPage::$PAGE_REGISTRATION?>'">
                регистрация
            </button>


        </div>
    </div>
  
</body>
</html>
