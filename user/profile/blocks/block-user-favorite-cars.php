<?php
    require_once $_SERVER['DOCUMENT_ROOT'] . "/resource/path.php";

    require_once $_SERVER['DOCUMENT_ROOT'] . PathFile::$OBJECT_CAR;
    require_once $_SERVER['DOCUMENT_ROOT'] . PathFile::$OBJECT_USER;
    $user = new User( (array_key_exists ( 'userId' , $_SESSION )) ? $_SESSION['userId'] : '');
?>

<div class="card-grey  pt-2 pb-2" style="width: 100%;" >
    <h4 class="ml-4" style="font-weight: bolder;">Понравившиеся авто</h4>

    <div class="d-flex flex-wrap justify-content-center card-in-card m-2">
        <?php
            $listFavoriteCarsIdStr = $user->getFavoriteCars();
            $listFavoriteCarsIdArr = explode(" ", $listFavoriteCarsIdStr);
            foreach ($listFavoriteCarsIdArr as $favoriteCarId){
                if ($favoriteCarId != '' && $favoriteCarId != null) {
                    $car = new Car($favoriteCarId);
                    $car->loadCarByIdFromDb();
                    if ($car->getId()) require $_SERVER['DOCUMENT_ROOT'] . PathFile::$ITEM_FAVORITE_CAR;
                }
            }
        ?>
    </div>

</div>