<!--на вход подаётся: $car(class Car)-->
<?php
    require_once $_SERVER['DOCUMENT_ROOT'] . "/resource/path.php";
?>

<a class="m-2 p-0" href="<?php echo PathTools::makePathToProjectRoot() . PathPage::$PAGE_CAR_INFORMATION?>?id_car=<?php echo $car->getId(); ?>" style="width: 20rem; " >
    <div class="d-flex align-items-center card-in-background card-in-background-hv p-2" style="width: 100%; height: 100%">
        <img src="<?php echo $car->getListPhotoPath()[0]; ?>" class="radius" style="width: 35%;" alt="car">

        <h5 class="p-2" style=" font-weight: bolder; text-align: center; width: 57%; align-content: center; height: min-content">
            <?php echo $car->getNameCar(); ?>
        </h5>

        <object style="width: 8%;">
            <a href="<?php echo PathTools::makePathToProjectRoot() . PathFile::$METHOD_FAVORITE_CAR?>?id_car=<?php echo $car->getId(); ?>&set_favorite=false" >
                <img src="<?php echo PathTools::makePathToProjectRoot() . 'img/favorite.png'?>" style="width: 100%;" alt="favorite">
            </a>
        </object>

    </div>
</a>