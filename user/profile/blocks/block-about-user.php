<?php
    require_once $_SERVER['DOCUMENT_ROOT'] . "/resource/path.php";
    require_once $_SERVER['DOCUMENT_ROOT'] . PathFile::$OBJECT_USER;
    $user = new User( (array_key_exists ( 'userId' , $_SESSION )) ? $_SESSION['userId'] : '');
?>

<div class="card-grey d-flex flex-wrap justify-content-center p-3" style="width: 100%;">
    <img src="<?php echo PathTools::makePathToProjectRoot() . 'img/profile.png'?>" style="width: 12rem; height: 12rem" alt="profile">

    <div class="container" style="width: calc(100% - 12rem); min-width: 16rem " >
        <h3 style="font-weight: bolder; text-align: center;">
            <?php echo $user->getName() . " " . $user->getSurname() ?>
        </h3>

        <div class="card-in-card p-3 mt-2" >
            <p style="font-weight: bold;" >
                Почта: <?php echo $user->getMail()?> <br>
                Логин: <?php echo $user->getLogin()?>
            </p>
        </div>

        <div class="mt-3 ml-auto mr-auto" style="width: 100%; max-width: 24rem;  ">
            <a href="<?php echo PathTools::makePathToProjectRoot() . PathFile::$METHOD_SIGN_OUT?>">
                <button type="button" class="btn btn-danger container">выйти</button>
            </a>
        </div>
    </div>
</div>