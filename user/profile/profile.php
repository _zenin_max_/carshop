<?php 
    session_start();
    require $_SERVER['DOCUMENT_ROOT'] . "/resource/path.php";
    require_once $_SERVER['DOCUMENT_ROOT'] . PathFile::$OBJECT_USER;
    require_once $_SERVER['DOCUMENT_ROOT'] . PathFile::$TABLE_DB_USERS;
    require_once $_SERVER['DOCUMENT_ROOT'] . PathFile::$TABLE_DB_CARS;

    $user = new User( (array_key_exists ( 'userId' , $_SESSION )) ? $_SESSION['userId'] : '');
    $page = PathPage::$PAGE_PROFILE;
    if (!$user->getId()) header("Location: " . PathTools::makePathToProjectRoot() . PathPage::$PAGE_SIGN_IN); //back
?>

<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Профиль</title>

    <?php require $_SERVER['DOCUMENT_ROOT'] . PathFile::$BLOCK_BOOTSTRAP; ?>

    <link rel="stylesheet" href="<?php echo PathTools::makePathToProjectRoot() . PathFile::$CSS_CARD?>">
    <link rel="stylesheet" href="<?php echo PathTools::makePathToProjectRoot() . PathFile::$CSS_MAIN?>">

</head>
<body>

    <?php require $_SERVER['DOCUMENT_ROOT'] . PathFile::$BLOCK_HEADER; ?>

    <div class="container d-flex flex-wrap">
        <div class="mb-4" style="width: 100%;">
            <?php require $_SERVER['DOCUMENT_ROOT'] . PathFile::$BLOCK_ABOUT_USER; ?>
        </div>
        <div class="mb-4" style="width: 100%;">
            <?php require $_SERVER['DOCUMENT_ROOT'] . PathFile::$BLOCK_USER_FAVORITE_CARS; ?>
        </div>
        
    </div>
    

</body>
</html>