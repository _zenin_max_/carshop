<?php

    $jsonUser = json_decode($_POST['json']);

    session_start();
    require $_SERVER['DOCUMENT_ROOT'] . "/resource/path.php";
    require $_SERVER['DOCUMENT_ROOT'] . PathFile::$OBJECT_USER;
    require_once $_SERVER['DOCUMENT_ROOT'] . PathFile::$STRING;
    require_once $_SERVER['DOCUMENT_ROOT'] . PathFile::$CALLBACK_OPERATION;

    $user = new User();

    $onFail = function ($status, $msg){
        echo json_encode(array(Status::$STATUS => $status, Status::$MESSAGE => $msg));
        exit();
    };
    $onSuccess = function ($msg){
        echo json_encode(array(Status::$STATUS => Status::$SUCCESS, Status::$MESSAGE => $msg));
        exit();
    };

    $user->setName($jsonUser->name, new CallbackOperation(null, $onFail));
    $user->setSurname($jsonUser->surname, new CallbackOperation(null, $onFail));

    $user->setLogin($jsonUser->login, new CallbackOperation(null, $onFail));

    $user->setPassword($jsonUser->password, $jsonUser->repeatPassword, new CallbackOperation(null, $onFail));
    $user->setMail($jsonUser->mail);


    $user->insertUserInDB(new CallbackOperation($onSuccess, $onFail));
