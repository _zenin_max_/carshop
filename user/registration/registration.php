<?php
    session_start();
    require $_SERVER['DOCUMENT_ROOT'] . "/resource/path.php";

    $page = PathPage::$PAGE_REGISTRATION;
?>

<!doctype html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Регистрация</title>

    <?php require $_SERVER['DOCUMENT_ROOT'] . PathFile::$BLOCK_BOOTSTRAP ?>

    <link rel="stylesheet" href="<?php echo PathTools::makePathToProjectRoot() . PathFile::$CSS_CARD?>">
    <link rel="stylesheet" href="<?php echo PathTools::makePathToProjectRoot() . PathFile::$CSS_MAIN?>">

</head>
<body>

    <?php require $_SERVER['DOCUMENT_ROOT'] . PathFile::$BLOCK_HEADER ?>

    <div class="container d-flex flex-wrap justify-content-center" style="width: 100%; min-width: 30rem;">
        <div class="color-text-1 m-5" style="width: 100%; text-align: center; font-weight: bolder; font-size: 2em;">
            Регистрация
        </div>


<!--        <form action="--><?php //echo PathTools::makePathToProjectRoot() . PathFile::$METHOD_REGISTRATION?><!--" method="POST" class="container d-flex flex-wrap justify-content-center align-self-center p-2">-->

            <div class="container d-flex justify-content-center align-self-center m-2 p-2 card-grey " style="width: 30rem; height: 4rem;">
                <div class="align-self-center color-text-2 m-2 " style="font-weight: bold; font-size: 1.4em;  width: 40%; text-align: center">
                    Имя
                </div>
                <input id="inpName" type="text" name="name" class="card-in-card p-2" style="outline: none; border: none; width: 60%; text-align: center; font-weight: bold">
            </div>

            <div class="container d-flex justify-content-center align-self-center m-2 p-2 card-grey " style="width: 30rem; height: 4rem;">
                <input id="inpSurname" type="text" name="surname" class="card-in-card p-2" style="outline: none; border: none; width: 60%; text-align: center;font-weight: bold" ">
                <div class="align-self-center color-text-2 m-2 " style="font-weight: bold; font-size: 1.4em;  width: 40%; text-align: center">
                    Фамилия
                </div>
            </div>

            <div class="container d-flex justify-content-center align-self-center m-2 p-2 card-grey " style="width: 30rem; height: 4rem;">
                <div class="align-self-center color-text-2 m-2 " style="font-weight: bold; font-size: 1.4em;  width: 40%; text-align: center">
                    Логин
                </div>
                <input id="inpLogin" type="text" name="login" class="card-in-card p-2" style="outline: none; border: none; width: 60%; text-align: center; font-weight: bold">
            </div>

            <div class="container d-flex justify-content-center align-self-center m-2 p-2 card-grey " style="width: 30rem; height: 4rem;">
                <input id="inpMail" type="email" name="mail" class="card-in-card p-2" style="outline: none; border: none; width: 60%; text-align: center;font-weight: bold" ">
                <div class="align-self-center color-text-2 m-2 " style="font-weight: bold; font-size: 1.4em;  width: 40%; text-align: center">
                    Email
                </div>
            </div>

            <div class="container d-flex justify-content-center align-self-center m-2 p-2 card-grey " style="width: 30rem; height: 4rem;">
                <div class="align-self-center color-text-2 m-2 " style="font-weight: bold; font-size: 1.4em;  width: 40%; text-align: center">
                    Пароль
                </div>
                <input id="inpPassword" type="password" name="password" class="card-in-card p-2" style="outline: none; border: none; width: 60%; text-align: center; font-weight: bold">
            </div>

            <div class="container d-flex justify-content-center align-self-center m-2 p-2 card-grey " style="width: 30rem; height: 4rem;">
                <input id="inpRepeatPassword" type="password" name="repeatPassword" class="card-in-card p-2" style="outline: none; border: none; width: 60%; text-align: center;font-weight: bold" ">
                <div class="align-self-center color-text-2 m-2 " style="font-weight: bold; font-size: 1.4em;  width: 40%; text-align: center; line-height: 0.9em">
                    Повторите пароль
                </div>
            </div>





            <div class="card-grey p-2 container d-flex flex-wrap justify-content-center align-self-center" style="width: 30rem;">
                <p id="pError" style="width: 100%; text-align: center; font-size: 1.0em; color: red; font-weight: bold; display: none"></p>
            </div>



            <div class="container d-flex justify-content-center align-self-center" style="width: 100%;  margin-top: 3%;" >
                <button class="btn-gray btn-gray-hv" style="font-weight: bold; font-size: 1.5em; width: 50%;" onclick="onClickCreateUser()">
                    ->
                </button>

            </div>



<!--        </form>-->

    </div>

</body>
</html>


<script src="<?php echo PathFile::$JS_JQUERY?>"></script>
<script>
    function onClickCreateUser() {
        let ob = {
            'name': document.getElementById("inpName").value,
            'surname': document.getElementById("inpSurname").value,
            'login': document.getElementById("inpLogin").value,
            'mail': document.getElementById("inpMail").value,
            'password': document.getElementById("inpPassword").value,
            'repeatPassword': document.getElementById("inpRepeatPassword").value
        };
        console.log(ob);
        jQuery.ajax({
            type: 'POST',
            url: '<?php echo PathFile::$METHOD_REGISTRATION?>',
            dataType: 'json',
            data: "json=" + JSON.stringify(ob),
            success: function (answer) {
                console.log("ok");
                console.log(answer);
                if (answer.<?php echo Status::$STATUS?> === '<?php echo Status::$SUCCESS ?>'){
                    location.href = '<?php echo PathTools::makePathToProjectRoot() . PathPage::$PAGE_SUCCESSFUL_REGISTRATION?>';

                    //let elementError = document.getElementById("pSuccess");
                    //elementError.innerText = answer.<?php //echo Status::$MESSAGE?>//;
                    //elementError.style.display = 'block';
                    console.log(answer.<?php echo Status::$STATUS?> + ': ' + answer.<?php echo Status::$MESSAGE?>);
                } else {
                    let elementError = document.getElementById("pError");
                    elementError.innerText = answer.<?php echo Status::$MESSAGE?>;
                    elementError.style.display = 'block';
                    console.log(answer.<?php echo Status::$STATUS?> + ': ' + answer.<?php echo Status::$MESSAGE?>);
                }
            }
        });
    }
</script>
