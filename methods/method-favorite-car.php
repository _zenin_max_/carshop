<?php
    session_start();
    require_once $_SERVER['DOCUMENT_ROOT'] . "/resource/path.php";
    require $_SERVER['DOCUMENT_ROOT'] . PathFile::$OBJECT_USER;

    $user = new User( (array_key_exists ( 'userId' , $_SESSION )) ? $_SESSION['userId'] : '');
?>
<?php

    if ($_GET['set_favorite'] == 'true'){
        $user->setFavoriteCars($user->getFavoriteCars() . " " . $_GET['id_car']);
        $user->updateUserInDb();
    }

    if ($_GET['set_favorite'] == 'false'){
        $user->setFavoriteCars(str_replace( $_GET['id_car'],'', $user->getFavoriteCars()));
        $user->setFavoriteCars(str_replace( '  ',' ', $user->getFavoriteCars()));
        $user->updateUserInDb();
    }

    header("Location: ".$_SERVER['HTTP_REFERER']); //back
?>