<!-- На вход подаётся: $page -->

<?php
    require_once $_SERVER['DOCUMENT_ROOT'] . "/resource/path.php";
    require_once $_SERVER['DOCUMENT_ROOT'] . PathFile::$TABLE_DB_CATEGORIES;
    require_once $_SERVER['DOCUMENT_ROOT'] . PathFile::$OBJECT_USER;
    $user = new User( (array_key_exists ( 'userId' , $_SESSION )) ? $_SESSION['userId'] : '');
?>

<nav class="navbar navbar-light card-grey mt-3 ml-2 mr-2 mb-4"  >
    <div class=" d-flex justify-content-between align-items-center m-0" style="width: 100%;">

        <div style="width: calc(100% / 3);">
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
        </div>

        <h4 class="m-auto" style="width: calc(100% / 3); height: min-content; text-align: center; font-weight: bold;">
            CarShop
        </h4>



        <div style="width: calc(100% / 3);" class="d-flex flex-row-reverse">
            <a href="<?php echo PathTools::makePathToProjectRoot() . (($user->getId()) ? PathPage::$PAGE_PROFILE : PathPage::$PAGE_SIGN_IN); ?>" style="width: 100%" >

                <div class=" d-flex align-items-center" style="width: 100%">
                    <h6 class="p-1 m-auto" style="font-weight: bolder; text-align: right; width: calc(100% - 2.5rem) ">
                        <?php echo ($user->getId()) ? $user->getName() : 'Гость'; ?>
                    </h6>

                    <img src="<?php echo PathTools::makePathToProjectRoot() . 'img/profile.png'; ?>" style="width: 2.5rem; height: 2.5rem" class="d-inline-block align-top" alt="" >

                </div>
            </a>
        </div>

    </div>

    <div class="collapse navbar-collapse" id="navbarNavDropdown">
        <ul class="navbar-nav " style="font-weight: bold;">

            <?php
                $tDbCategories = new TableDbCategories();
                $list_categories = $tDbCategories->getListCategories();
                foreach($list_categories as $category){ ?>
                <li class="nav-item <?php $path = $category['name_for_system']; echo ($page == PathPage::$$path) ? 'active' : ''; ?>">
                    <a class="nav-link" href="<?php $path = $category['name_for_system']; echo PathTools::makePathToProjectRoot() . PathPage::$$path; ?>"> <?php echo $category['name_ru']; ?> </a>
                </li>
            <?php } ?>

        </ul>
    </div>
</nav>
