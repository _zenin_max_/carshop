<!-- на вход подаётся: $listPhotoRes-->

<div id="sliderPhoto" class="carousel slide card-grey" style="width: 100%;" data-ride="carousel">
    <div class="container d-flex justify-content-center">
        
        <a class="carousel-control-prev" href="#sliderPhoto" role="button" style="width: 3.5%;" data-slide="prev">
            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
            <span clss="sr-only"></span>
        </a>
        <div class="carousel-inner m-0 p-0" style="width: 96%;">
            <?php   $i = 0;
                    foreach($listPhotoRes as $photoRes){
            ?>
                        <div class="carousel-item d-flex justify-content-center <?php echo ($i == 0) ? 'active' : ''?> ">
                            <img src="<?php echo PathTools::makePathToProjectRoot() . $photoRes ?>" class="d-block" alt="photo"
                                 style="width: auto; height: auto; max-width: 100%; max-height: 40rem">
                        </div>
            <?php       $i++;
                    }   
            ?>
        </div>
        <a class="carousel-control-next m-0 p-0" href="#sliderPhoto" role="button" style="width: 3.5%;" data-slide="next">
            <span class="carousel-control-next-icon" aria-hidden="true"></span>
            <span class="sr-only"></span>
        </a>
    </div>
</div>

