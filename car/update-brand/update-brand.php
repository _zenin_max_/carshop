<?php
    session_start();
    require $_SERVER['DOCUMENT_ROOT'] . "/resource/path.php";
    require $_SERVER['DOCUMENT_ROOT'] .  PathFile::$STRING;
    require_once $_SERVER['DOCUMENT_ROOT'] . PathFile::$OBJECT_USER;

    $page = PathPage::$PAGE_UPDATE_BRAND;
    $user = new User( (array_key_exists ( 'userId' , $_SESSION )) ? $_SESSION['userId'] : '');

    if ($user->getPrivilege() != 1 ) header("Location: " . PathTools::makePathToProjectRoot() . PathPage::$PAGE_LIST_CAR);

    $brand = new CarBrand($_POST['idBrand']);
    $brand->loadCarBrandFromDbById();
?>

<!doctype html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Редактировать бренд</title>

    <?php require $_SERVER['DOCUMENT_ROOT'] . PathFile::$BLOCK_BOOTSTRAP; ?>

    <link rel="stylesheet" href="<?php echo PathTools::makePathToProjectRoot() . PathFile::$CSS_CARD?>">
    <link rel="stylesheet" href="<?php echo PathTools::makePathToProjectRoot() . PathFile::$CSS_MAIN?>">
    <link rel="stylesheet" href="<?php echo PathTools::makePathToProjectRoot() . PathFile::$CSS_BUTTON?>">

</head>
<body>

    <?php require $_SERVER['DOCUMENT_ROOT'] . PathFile::$BLOCK_HEADER; ?>

    <div class="container card-grey">
        <h2 class="p-2">Редактирование бренда</h2>
        <div class="d-flex justify-content-center align-self-stretch flex-wrap">
            <?php
                $title = 'Название бренда';
                $idTextarea = 'taNameBrand';
                $valTextarea = $brand->getNameBrand();
                require $_SERVER['DOCUMENT_ROOT'] . PathFile::$ITEM_TEXT_OPERATION;

                $title = 'Информация о бренде';
                $idTextarea = 'taAboutBrand';
                $valTextarea = $brand->getAboutBrand();
                require $_SERVER['DOCUMENT_ROOT'] . PathFile::$ITEM_TEXT_OPERATION;
            ?>
        </div>

        <p id="pError" style="display: none; color: red; font-weight: bold; text-align: center"></p>
        <p id="pSuccess" style="display: none; color: green; font-weight: bold; text-align: center"></p>
        <div class="d-flex justify-content-center p-2" style="width: auto">
            <button onclick="onClickUpdateBrand()" class="button-blue" style="font-size: 1.5rem" >
                Обновить
            </button>
        </div>
    </div>
</body>
</html>

<script src="<?php echo PathFile::$JS_JQUERY?>"></script>
<script>
    function onClickUpdateBrand() {
        let ob = {
            'idBrand': <?php echo $brand->getId()?>,
            'nameBrand': document.getElementById("taNameBrand").value,
            'aboutBrand': document.getElementById("taAboutBrand").value
        };
        jQuery.ajax({
            type: 'POST',
            url: '<?php echo PathFile::$METHOD_UPDATE_BRAND?>',
            dataType: 'json',
            data: "json=" + JSON.stringify(ob),
            success: function (answer) {
                if (answer.<?php echo Status::$STATUS?> === '<?php echo Status::$SUCCESS ?>'){
                    let elementError = document.getElementById("pSuccess");
                    elementError.innerText = answer.<?php echo Status::$MESSAGE?>;
                    elementError.style.display = 'block';
                    console.log(answer.<?php echo Status::$STATUS?> + ': ' + answer.<?php echo Status::$MESSAGE?>);
                } else {
                    let elementError = document.getElementById("pError");
                    elementError.innerText = answer.<?php echo Status::$MESSAGE?>;
                    elementError.style.display = 'block';
                    console.log(answer.<?php echo Status::$STATUS?> + ': ' + answer.<?php echo Status::$MESSAGE?>);
                }
            }
        });
    }
</script>