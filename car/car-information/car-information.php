<?php
    session_start();
    require $_SERVER['DOCUMENT_ROOT'] . "/resource/path.php";
    require_once $_SERVER['DOCUMENT_ROOT'] . PathFile::$TABLE_DB_USERS;
    require_once $_SERVER['DOCUMENT_ROOT'] . PathFile::$TABLE_DB_CARS;
    require_once $_SERVER['DOCUMENT_ROOT'] . PathFile::$OBJECT_CAR;
    require_once $_SERVER['DOCUMENT_ROOT'] . PathFile::$OBJECT_USER;

    $user = new User( (array_key_exists ( 'userId' , $_SESSION )) ? $_SESSION['userId'] : '');
    $page = PathPage::$PAGE_CAR_INFORMATION;
    $car = new Car($_GET['id_car']);
    $car->loadCarByIdFromDb();
?>

<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title><?php echo $car->getNameCar(); ?></title>

    <?php require $_SERVER['DOCUMENT_ROOT'] . PathFile::$BLOCK_BOOTSTRAP; ?>

    <link rel="stylesheet" href="<?php echo PathTools::makePathToProjectRoot() . PathFile::$CSS_CARD?>">
    <link rel="stylesheet" href="<?php echo PathTools::makePathToProjectRoot() . PathFile::$CSS_MAIN?>">

</head>
<body>

    <?php require $_SERVER['DOCUMENT_ROOT'] . PathFile::$BLOCK_HEADER; ?>

    <div class="container d-flex flex-wrap justify-content-center">

        <p class="mb-5 mt-5 color-text-1" style="width: 100%; text-align: center; font-size: 4em; font-weight: bolder;">
            <?php echo $car->getNameCar()?>
        </p>

        <div class="mb-5 mt-4" style="width: 100%;">
            <?php 
                $listPhotoRes = $car->getListPhotoPath();
                require $_SERVER['DOCUMENT_ROOT'] . PathFile::$BLOCK_SLIDER_PHOTOS;
            ?>
        </div>

        <div class="mb-5" style="width: 100%">
            <?php require $_SERVER['DOCUMENT_ROOT'] . PathFile::$BLOCK_ABOUT_CAR ?>
        </div>

        <div class="card-grey mb-5" style="width: 100%;">
            <p class="color-text-2 mt-2 ml-4" style="font-size: 2em; font-weight: bold;">
                Отзывы
            </p>
            <?php   if ($user->getId()){ ?>
                        <div class="p-3" style="width: 100%;">
                            <?php require $_SERVER['DOCUMENT_ROOT'] . PathFile::$BLOCK_CREATE_REVIEW_CAR?>
                        </div>
            <?php   } ?>

            <div class="p-3" style="width: 100%;">
                <?php require $_SERVER['DOCUMENT_ROOT'] . PathFile::$BLOCK_REVIEWS_CAR ?>
            </div>
        </div>

    </div>
</body>
</html>














