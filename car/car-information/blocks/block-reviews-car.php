<?php
    require_once $_SERVER['DOCUMENT_ROOT'] . "/resource/path.php";
    require_once $_SERVER['DOCUMENT_ROOT'] . PathFile::$TABLE_DB_USERS;
    require_once $_SERVER['DOCUMENT_ROOT'] . PathFile::$TABLE_DB_REVIEWS_ABOUT_CAR;
    require_once $_SERVER['DOCUMENT_ROOT'] . PathFile::$OBJECT_USER;
    require_once $_SERVER['DOCUMENT_ROOT'] . PathFile::$OBJECT_LIST_REVIEWS_CAR;


    $listReviewCar = new ListReviewsCar();
    $listReviewCar = $listReviewCar->getListReviewsByIdCar($car->getId());
?>

<?php   if (!$listReviewCar){ ?>
            <p class="card-in-card p-3" style="text-align: center; ">
                Оставьте отзыв об этой машине первым!
            </p>
<?php   }   ?>

<?php   if (!array_key_exists( 'userId' , $_SESSION )){ ?>
            <p class="card-in-card p-3" style="text-align: center; ">
                Чтобы оставить отзыв нужно войти в аккаунт
            </p>
<?php   }   ?>

<?php   if ($listReviewCar) {
            foreach ($listReviewCar as $reviewCar) {
                require $_SERVER['DOCUMENT_ROOT'] . PathFile::$ITEM_REVIEW;
            }
        }
?>

