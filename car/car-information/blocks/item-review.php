<!-- На вход подаётся: $reviewId , $car(class Car)-->
<?php
    require_once $_SERVER['DOCUMENT_ROOT'] . "/resource/path.php";
    require_once $_SERVER['DOCUMENT_ROOT'] . PathFile::$OBJECT_REVIEW_CAR;
    require_once $_SERVER['DOCUMENT_ROOT'] . PathFile::$OBJECT_CAR;
    require_once $_SERVER['DOCUMENT_ROOT'] . PathFile::$OBJECT_USER;

    $user = new User( (array_key_exists ( 'userId' , $_SESSION )) ? $_SESSION['userId'] : '');
    $userWhoLeftReview = new User($reviewCar->getIdUser());
?>

<div class="d-flex flex-column" style="margin-top: 2%;">
    <div class="d-flex flex-wrap align-items-center" style="width: 100%;">
        <img src="<?php echo PathTools::makePathToProjectRoot() . 'img/profile.png'?>" alt="profile" style="width: 3rem; height: 3rem;" >

        <h6  class="p-1" style="height: min-content; font-weight: bolder; width: calc(100% - 3rem);">
            <?php echo $userWhoLeftReview->getName() . ' ' . $userWhoLeftReview->getSurname()
                . (($user->getId() === $userWhoLeftReview->getId()) ? ' (мой отзыв)' : '') . "<br>"
                . $reviewCar->getDate();?>
        </h6>
    </div>

    <div style="width: calc(100% - 3rem); margin-left: 3rem;">

        <p class="card-in-card p-3 m-0" style="width: 100%; font-weight: bold; border-radius: <?php echo ($user->getId() === $userWhoLeftReview->getId()) ? '0 12px 0 0': '0 12px 12px 12px' ?>; ">
            Оценка: <?php echo $reviewCar->getRating()?> / 10
            <br>
            Отзыв:
            <?php echo $reviewCar->getReview(); ?>
        </p>

        <?php   if ($user->getId() === $userWhoLeftReview->getId()){  ?>
            <button onclick="onClickDeleteReview('<?php echo $reviewCar->getId(); ?>')" class="card-red card-red-hv m-0" style="font-weight: bold; font-size: 1.2em; width: 100%; border-radius: 0 0 12px 12px ">
                X
            </button>
        <?php   } ?>

    </div>

</div>

<script>
    function onClickDeleteReview(idReview) {
        let ob = {'idReview': idReview};
        jQuery.ajax({
            type: 'POST',
            url: '<?php echo PathFile::$METHOD_DELETE_REVIEW_CAR?>',
            dataType: 'json',
            data: "json=" + JSON.stringify(ob),
            success: function (answer) {
                if (answer.<?php echo Status::$STATUS?> === '<?php echo Status::$SUCCESS ?>'){
                    console.log(answer.<?php echo Status::$STATUS?> + ': ' + answer.<?php echo Status::$MESSAGE?>);
                    location.reload();
                } else {
                    console.log(answer.<?php echo Status::$STATUS?> + ': ' + answer.<?php echo Status::$MESSAGE?>);
                }
            }
        });
    }
</script>