<?php
    require_once $_SERVER['DOCUMENT_ROOT'] . "/resource/path.php";
    require_once $_SERVER['DOCUMENT_ROOT'] . PathFile::$STRING;
    require_once $_SERVER['DOCUMENT_ROOT'] . PathFile::$OBJECT_USER;
    require_once $_SERVER['DOCUMENT_ROOT'] . PathFile::$OBJECT_REVIEW_CAR;

    $user = new User( (array_key_exists ( 'userId' , $_SESSION )) ? $_SESSION['userId'] : '');
?>

<link rel="stylesheet" href="<?php echo PathFile::$CSS_CREATE_REVIEW?>">
<script src="<?php echo PathFile::$JS_JQUERY?>"></script>
<script src="<?php echo PathFile::$JS_UTILITY?>"></script>

<div class="d-flex flex-column">

    <div class="d-flex flex-wrap align-items-center" style="width: 100%;">
        <img src="<?php echo PathTools::makePathToProjectRoot() . 'img/profile.png'?>" alt="profile" style="width: 3rem; height: 3rem" >
        <h6 class="p-1" style="height: min-content; font-weight: bolder; width: calc(100% - 3rem);">
            <?php echo $user->getName() . ' ' . $user->getSurname() ; ?>
            <br>
            (напишите свой отзыв)
        </h6>
    </div>

    <div class="d-flex flex-wrap justify-content-center " style="width: calc(100% - 3rem); margin-left: 3rem; ">

        <div class="card-in-card pt-2 pl-4 pr-4" style="width: 100%; border-top-left-radius: 0;">

            <div class="d-flex flex-wrap" style="width: 100%;">
                <p class="m-0" style="font-weight: bold" >
                    Оценка:
                </p>

                <div class="d-flex justify-content-center ml-2" style="width: 16rem;" oninput="setValueRangeInElement('rangeRating', 'valueRating')">
                    <input type="range" id="rangeRating" min="<?php echo ReviewCar::$minRating?>" name="rating" max="<?php echo ReviewCar::$maxRating?>" step="<?php echo ReviewCar::$stepRating?>" style="width: 80%; ">
                    <p class="m-auto" id="valueRating" style="width: 20%;
                        text-align: center; font-weight: bold">5.0</p>
                </div>
            </div>

            <div class="d-flex flex-wrap" style="width: 100%;">
                <p class="m-0" id="pTitleReview" style="font-weight: bold;"> Отзыв: </p>
                <textarea id="taReview" class="mb-2" name="review" style=" background: transparent;  color: inherit" ></textarea>
            </div>

            <p id="pError" style="width: 100%; text-align: center; display: none; color: red"></p>

        </div>

        <button  id="btnCreateReview" onclick="onClickCreateReview()" class="btn-create-review"  style="width: 90%; min-width: 40px; text-align: center">
            ->
        </button>

    </div>
</div>

<script>
    window.onresize = resize;
    function resize() {
        let widthElementTitleReview = document.getElementById("pTitleReview").offsetWidth;
        let elementReview = document.getElementById("taReview");
        elementReview.style.marginLeft = "10px";
        elementReview.style.width = "calc(100% - " + elementReview.style.marginLeft +
            " - " + widthElementTitleReview + "px)";
        if (elementReview.offsetWidth < 250){
            elementReview.style.marginLeft = "0px";
            elementReview.style.width = "100%";
        }
    }
    resize()
</script>

<script>
    function onClickCreateReview() {
        let ob = {
            'review': document.getElementById("taReview").value,
            'rating': document.getElementById("rangeRating").value,
            'idCar': <?php echo $car->getId(); ?>
        }
        jQuery.ajax({
            type: 'POST',
            url: '<?php echo PathFile::$METHOD_CREATE_REVIEW_CAR?>',
            dataType: 'json',
            data: "json=" + JSON.stringify(ob),
            success: function (answer) {
                if (answer.<?php echo Status::$STATUS?> === '<?php echo Status::$SUCCESS ?>'){
                    console.log(answer.<?php echo Status::$STATUS?> + ': '
                        + answer.<?php echo Status::$MESSAGE?>);
                    location.reload();
                } else {
                    let elementError = document.getElementById("pError");
                    elementError.innerText = answer.<?php echo Status::$MESSAGE?>;
                    elementError.style.display = 'block';
                    console.log(answer.<?php echo Status::$STATUS?> + ': '
                        + answer.<?php echo Status::$MESSAGE?>);
                }
            }
        });
    }
</script>
