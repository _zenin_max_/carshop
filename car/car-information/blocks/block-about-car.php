<!-- На вход подаётся: $car(class Car)-->

<div class="card-grey p-2" style="width: 100%;">
    <p class="color-text-2 mt-2 ml-4" style="font-size: 2em; font-weight: bold;">
        Характеристика
    </p>
    <div class="d-flex flex-wrap justify-content-center ">
        <?php
            foreach ($car->getListCharacteristicWithValue() as $characteristic){
                $itemInItem = array('text' => $characteristic['characteristic'], 'value' => $characteristic['value']);
                require $_SERVER['DOCUMENT_ROOT'] . PathFile::$ITEM_IN_ITEM;
            }
        ?>
    </div>
</div>
