<!-- На вход подаётся: $itemInItem = array('text' => ... , 'value' => ...) -->

<div class="container d-flex flex-wrap justify-content-center card-in-card p-3 m-1" style="text-align: center;
    width: 20rem; ">
    <h6 style="width: 100%;"><?php echo $itemInItem['text'];?></h6>
    <h2 style="width: 100%;"><?php echo $itemInItem['value'];?></h2>
</div>