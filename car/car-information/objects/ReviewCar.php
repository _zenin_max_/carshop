<?php
    require_once $_SERVER['DOCUMENT_ROOT'] . "/resource/path.php";
    require_once $_SERVER['DOCUMENT_ROOT'] . PathFile::$STRING;
    require_once $_SERVER['DOCUMENT_ROOT'] . PathFile::$TABLE_DB_REVIEWS_ABOUT_CAR;
    require_once $_SERVER['DOCUMENT_ROOT'] . PathFile::$CALLBACK_OPERATION;

    class ReviewCar{
        private $id;
        private $idUser;
        private $review;
        private $date;
        private $idCar;
        private $rating;

        static $minRating = 0;
        static $maxRating = 10;
        static $stepRating = 0.1;

        private $tDbReviewsAboutCar;

        public function __construct(){
            $this->tDbReviewsAboutCar = new TableDbReviewsAboutCar();
        }

        public function loadReviewByIdFromDB(){
            $reviewFromDb = array();
            if ($this->id != '' || $this->id != null)
                $reviewFromDb = $this->tDbReviewsAboutCar->getReviewById($this->id);
            if ($reviewFromDb){
                $this->id = $reviewFromDb[$this->tDbReviewsAboutCar->getCnId()];
                $this->idUser = $reviewFromDb[$this->tDbReviewsAboutCar->getCnIdUser()];
                $this->review = $reviewFromDb[$this->tDbReviewsAboutCar->getCnReview()];
                $this->date = $reviewFromDb[$this->tDbReviewsAboutCar->getCnDate()];
                $this->idCar = $reviewFromDb[$this->tDbReviewsAboutCar->getCnIdCar()];
                $this->rating = $reviewFromDb[$this->tDbReviewsAboutCar->getCnRating()];
            }
        }

        public function updateReviewInDB(CallbackOperation $callbackOperations){
            $this->tDbReviewsAboutCar->updateReviewCar($this->id, $this->review, $this->date, $this->rating, $this->idCar, $callbackOperations);
        }

        public function deleteReviewInDB(CallbackOperation $callbackOperations){
            $this->tDbReviewsAboutCar->deleteReviewCar($this->id, $callbackOperations);
        }

        public function insertReviewInDB(CallbackOperation $callbackOperations){
            $this->tDbReviewsAboutCar->insertReviewCar($this->idUser, $this->review, $this->date, $this->rating, $this->idCar, $callbackOperations);
        }

        public function getRating(){
            return $this->rating;
        }
        public function setRating($rating, CallbackOperation $callbackOperations = null){
            if ($rating >= ReviewCar::$minRating && $rating <= ReviewCar::$maxRating) {
                $this->rating = $rating;
                if ($callbackOperations != null) ($callbackOperations->onSuccess)();
            } else {
                if ($callbackOperations != null) ($callbackOperations->onFail)( Status::$WRONG_FORMAT, "Неверный формат рейтинга");
            }
        }

        public function getId(){
            return $this->id;
        }
        public function setId($id){
            $this->id = $id;
        }

        public function getIdUser(){
            return $this->idUser;
        }
        public function setIdUser($idUser){
            $this->idUser = $idUser;
        }

        public function getReview(){
            return $this->review;
        }
        public function setReview($review, CallbackOperation $callbackOperations = null){
            if (!empty($review)){
                $this->review = $review;
                if($callbackOperations != null) ($callbackOperations->onSuccess)();
            } else {
                if($callbackOperations != null) ($callbackOperations->onFail)(Status::$WRONG_FORMAT, "Поле с отзывом пустое");
            }
        }

        public function getDate(){
            return $this->date;
        }
        public function setDate($date){
            $this->date = $date;
        }

        public function getIdCar(){
            return $this->idCar;
        }
        public function setIdCar($idCar){
            $this->idCar = $idCar;
        }

    }