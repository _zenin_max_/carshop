<?php

    require_once $_SERVER['DOCUMENT_ROOT'] . "/resource/path.php";
    require_once $_SERVER['DOCUMENT_ROOT'] . PathFile::$TABLE_DB_REVIEWS_ABOUT_CAR;
    require_once $_SERVER['DOCUMENT_ROOT'] . PathFile::$OBJECT_REVIEW_CAR;

    class ListReviewsCar{

        private $tDbReviewsAboutCar;

        public function __construct(){
            $this->tDbReviewsAboutCar = new TableDbReviewsAboutCar();
        }

        public function getListReviewsByIdCar($idCar){
            $listReviews = array();
            $listReviewsFromDb = $this->tDbReviewsAboutCar->getListReviewByIdCar($idCar);
            foreach ($listReviewsFromDb as $reviewFromDb){
                $review = new ReviewCar();
                $review->setId($reviewFromDb[$this->tDbReviewsAboutCar->getCnId()]);
                $review->setIdUser($reviewFromDb[$this->tDbReviewsAboutCar->getCnIdUser()]);
                $review->setIdCar($reviewFromDb[$this->tDbReviewsAboutCar->getCnIdCar()]);
                $review->setReview($reviewFromDb[$this->tDbReviewsAboutCar->getCnReview()], null);
                $review->setRating($reviewFromDb[$this->tDbReviewsAboutCar->getCnRating()], null);
                $review->setDate($reviewFromDb[$this->tDbReviewsAboutCar->getCnDate()]);
                array_push($listReviews, $review);
            }
            return $listReviews;
        }


    }