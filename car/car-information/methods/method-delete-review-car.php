<?php
    $jsonReview = json_decode($_POST['json']);

    session_start();
    require_once $_SERVER['DOCUMENT_ROOT'] . "/resource/path.php";
    require_once $_SERVER['DOCUMENT_ROOT'] . PathFile::$STRING;
    require_once $_SERVER['DOCUMENT_ROOT'] . PathFile::$OBJECT_REVIEW_CAR;
    require_once $_SERVER['DOCUMENT_ROOT'] . PathFile::$CALLBACK_OPERATION;

    $reviewCar = new ReviewCar();
    $reviewCar->setId($jsonReview->idReview);
    $reviewCar->deleteReviewInDB(new CallbackOperation(null, null));

    echo json_encode(array(Status::$STATUS => Status::$SUCCESS));

    exit();