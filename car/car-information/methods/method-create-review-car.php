<?php

    $jsonReview = json_decode($_POST['json']);

    session_start();
    require_once $_SERVER['DOCUMENT_ROOT'] . "/resource/path.php";
    require_once $_SERVER['DOCUMENT_ROOT'] . PathFile::$STRING;
    require_once $_SERVER['DOCUMENT_ROOT'] . PathFile::$OBJECT_REVIEW_CAR;
    require_once $_SERVER['DOCUMENT_ROOT'] . PathFile::$OBJECT_SWEAR_WORD;
    require_once $_SERVER['DOCUMENT_ROOT'] . PathFile::$CALLBACK_OPERATION;

    $reviewCar = new ReviewCar();
    $checkReviewAnSwearWord = new SwearWord($jsonReview->review);
    $checkReviewAnSwearWord->removeSwearWordsFromText();

    $onFail = function ($status, $msg){
        echo json_encode(array(Status::$STATUS => $status, Status::$MESSAGE => $msg));
        exit();
    };
    $onSuccess = function ($status, $msg){
        echo json_encode(array(Status::$STATUS => $status, Status::$MESSAGE => $msg));
        exit();
    };

    $reviewCar->setIdUser($_SESSION['userId']);
    $reviewCar->setReview($checkReviewAnSwearWord->getText(),  new CallbackOperation(null, $onFail));
    $reviewCar->setDate(date("Y-m-d H:i:s"));
    $reviewCar->setIdCar($jsonReview->idCar);
    $reviewCar->setRating($jsonReview->rating, new CallbackOperation(null, $onFail));

    $reviewCar->insertReviewInDB(new CallbackOperation($onSuccess, $onFail));

    exit();

//    header("Location: " . $_SERVER['HTTP_REFERER']); //back

