<?php

    $jsonBrand = json_decode($_POST['json']);

    session_start();
    require_once $_SERVER['DOCUMENT_ROOT'] . "/resource/path.php";
    require_once $_SERVER['DOCUMENT_ROOT'] . PathFile::$STRING;
    require_once $_SERVER['DOCUMENT_ROOT'] . PathFile::$OBJECT_CAR_BRAND;
    require_once $_SERVER['DOCUMENT_ROOT'] . PathFile::$CALLBACK_OPERATION;

    $carBrand = new CarBrand();

    $onFail = function ($status, $msg){
        echo json_encode(array(Status::$STATUS => $status, Status::$MESSAGE => $msg));
        exit();
    };
    $onSuccess = function ($msg){
        echo json_encode(array(Status::$STATUS => Status::$SUCCESS, Status::$MESSAGE => $msg));
        exit();
    };

    $carBrand->setNameBrand($jsonBrand->nameBrand);
    $carBrand->setAboutBrand($jsonBrand->aboutBrand);

    $carBrand->insertCarBrandInDB(new CallbackOperation($onSuccess, $onFail));

    exit();

