<!-- На вход подаётся: CarBrand(class) -->
<?php
    require_once $_SERVER['DOCUMENT_ROOT'] . "/resource/path.php";
    require_once $_SERVER['DOCUMENT_ROOT'] . PathFile::$OBJECT_CAR_BRAND;

?>
<div class="card card-grey container-fluid mb-4 " style="width: 20rem;" >
    <img class="container card-img-top mt-3 mb-2 "
         src="<?php echo $carBrand->getPathLogo(); ?>"
         style="width: 60%;" alt="Brand">

    <div class="card-body">
        <p class="card-title " style="color: black; font-weight: bolder; text-align: center; font-size: 1.3em;">
            <?php echo $carBrand->getNameBrand(); ?>
        </p>
        <p> <?php echo $carBrand->getAboutBrand(); ?> </p>
    </div>
</div>