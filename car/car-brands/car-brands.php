<?php 
    session_start();
    require_once $_SERVER['DOCUMENT_ROOT'] . "/resource/path.php";
    require_once $_SERVER['DOCUMENT_ROOT'] . PathFile::$TABLE_DB_USERS;
    require_once $_SERVER['DOCUMENT_ROOT'] . PathFile::$OBJECT_USER;
    require_once $_SERVER['DOCUMENT_ROOT'] . PathFile::$OBJECT_CAR_BRAND;
    require_once $_SERVER['DOCUMENT_ROOT'] . PathFile::$OBJECT_LIST_CAR_BRANDS;
    require_once $_SERVER['DOCUMENT_ROOT'] . PathFile::$TABLE_DB_CAR_BRANDS;

    $user = new User( (array_key_exists ( 'userId' , $_SESSION )) ? $_SESSION['userId'] : '');
    $page = PathPage::$PAGE_CAR_BRANDS;
?>

<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Проиводители автомобилей</title>

    <?php require $_SERVER['DOCUMENT_ROOT'] . PathFile::$BLOCK_BOOTSTRAP; ?>

    <link rel="stylesheet" href="<?php echo PathTools::makePathToProjectRoot() . PathFile::$CSS_CARD?>">
    <link rel="stylesheet" href="<?php echo PathTools::makePathToProjectRoot() . PathFile::$CSS_MAIN?>">

</head>
<body>
  
    <?php require $_SERVER['DOCUMENT_ROOT'] . PathFile::$BLOCK_HEADER; ?>

    <div class=" container d-flex flex-wrap">
        <?php
            $listCarBrands = new ListCarBrands();
            foreach($listCarBrands->getListAllCarBrands() as $carBrand){
                require $_SERVER['DOCUMENT_ROOT'] . PathFile::$ITEM_CAR_BRAND;
            }
        ?>
    </div>
    
</body>
</html>