<?php

    $jsonCar = json_decode($_POST['json']);

    session_start();
    require_once $_SERVER['DOCUMENT_ROOT'] . "/resource/path.php";
    require_once $_SERVER['DOCUMENT_ROOT'] . PathFile::$STRING;
    require_once $_SERVER['DOCUMENT_ROOT'] . PathFile::$CALLBACK_OPERATION;
    require_once $_SERVER['DOCUMENT_ROOT'] . PathFile::$OBJECT_CAR;

    $car = new Car($jsonCar->idCar);
    $car->loadCarByIdFromDb();

    $onFail = function ($status, $msg){
        echo json_encode(array(Status::$STATUS => $status, Status::$MESSAGE => $msg));
        exit();
    };
    $onSuccess = function ($status, $msg){
        echo json_encode(array(Status::$STATUS => $status, Status::$MESSAGE => $msg));
        exit();
    };

    $car->setNameCar($jsonCar->nameCar);
    $car->setCountryOfManufacture($jsonCar->countryOfManufacture);
    $car->setMaximumSpeed($jsonCar->maximumSpeed);
    $car->setAccelerationTo100($jsonCar->accelerationTo100);

    $car->updateCarInDB(new CallbackOperation($onSuccess, $onFail));

    exit();

