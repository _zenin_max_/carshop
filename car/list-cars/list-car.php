<?php 
    session_start();
    require $_SERVER['DOCUMENT_ROOT'] . "/resource/path.php";
    require_once $_SERVER['DOCUMENT_ROOT'] . PathFile::$TABLE_DB_USERS;
    require_once $_SERVER['DOCUMENT_ROOT'] . PathFile::$TABLE_DB_CARS;
    require_once $_SERVER['DOCUMENT_ROOT'] . PathFile::$TABLE_DB_CAR_BRANDS;
    require_once $_SERVER['DOCUMENT_ROOT'] . PathFile::$OBJECT_USER;
    require_once $_SERVER['DOCUMENT_ROOT'] . PathFile::$OBJECT_LIST_CAR_BRANDS;
    require_once $_SERVER['DOCUMENT_ROOT'] . PathFile::$OBJECT_CAR_BRAND;
    require_once $_SERVER['DOCUMENT_ROOT'] . PathFile::$OBJECT_LIST_CARS;

    $page = PathPage::$PAGE_LIST_CAR;
    $user = new User( (array_key_exists ( 'userId' , $_SESSION )) ? $_SESSION['userId'] : '');
?>

<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Список авто</title>

    <?php require $_SERVER['DOCUMENT_ROOT'] . PathFile::$BLOCK_BOOTSTRAP; ?>

    <link rel="stylesheet" href="<?php echo PathTools::makePathToProjectRoot() . PathFile::$CSS_CARD?>">
    <link rel="stylesheet" href="<?php echo PathTools::makePathToProjectRoot() . PathFile::$CSS_MAIN?>">
    <link rel="stylesheet" href="<?php echo PathTools::makePathToProjectRoot() . PathFile::$CSS_LIST_CAR?>">

</head>
<body>
    
    <?php require $_SERVER['DOCUMENT_ROOT'] . PathFile::$BLOCK_HEADER; ?>

    <div class="container">

        <div class="card-grey pt-2 pb-2">
            <h3 class="ml-4">
                Машины
            </h3>
            <div class="d-flex align-items-stretch scrollbar-primary mt-1 mb-2" style="overflow-x: auto; width: 100%">

                <a class="<?php echo (array_key_exists('id_brand', $_GET) || array_key_exists('search', $_GET)) ? 'car-brand' : 'select-car-brand'?> d-flex align-items-center"
                   href="<?php echo PathTools::makePathToProjectRoot() . PathPage::$PAGE_LIST_CAR?>" style="font-weight: bold">
                    <h6>ALL</h6>
                </a>

                <div class="<?php echo (array_key_exists('search', $_GET)) ? 'select-car-brand' : 'car-brand'?> d-flex align-items-center">
                    <form action="<?php echo PathTools::makePathToProjectRoot() . PathPage::$PAGE_LIST_CAR?>" method="get"
                        style="width: calc(100%-2rem)" id="searchForm" class="p-1">
                        <textarea name="search" class="card-in-card" style="font-weight: bold; text-align: center"><?php echo (array_key_exists('search', $_GET)) ? $_GET['search'] : ''?></textarea>
                    </form>
                    <button form="searchForm" class="p-2">
                        <img src="<?php echo PathTools::makePathToProjectRoot() . 'img/search.png'?>" style="width: 1.5rem; ">
                    </button>
                </div>

                <?php
                    $listCarBrands = new ListCarBrands();
                    foreach($listCarBrands->getListAllCarBrands() as $carBrand){
                        require $_SERVER['DOCUMENT_ROOT'] . PathFile::$ITEM_MINI_CAR_BRAND;
                    }
                ?>
                <?php   if ($user->getPrivilege() == 1){    ?>
                            <a href="<?php echo PathTools::makePathToProjectRoot() . PathPage::$PAGE_CREATE_BRAND?>" class="button-create p-2"
                               style="align-self: center;">
                                <img src="<?php echo PathTools::makePathToProjectRoot() . 'img/add.png'?>" style="width: 3rem; filter: invert(1);" alt="add" >
                            </a>
                <?php   } ?>

            </div>

            <div class="card-in-card ml-2 mr-2 mt-1 mb-1">
                <div class="d-flex flex-wrap justify-content-center">
                    <?php
                        $listCars = new ListCars();
                        if (array_key_exists('search', $_GET)){
                            $listCars = $listCars->searchCarsByWord($_GET['search']);
                        } elseif (array_key_exists('id_brand', $_GET)){
                            $listCars = $listCars->getListCarsByIdBrand($_GET['id_brand']);
                        } else {
                            $listCars = $listCars->getAllCars();
                        }
                        foreach($listCars as $car){
                            require $_SERVER['DOCUMENT_ROOT'] . PathFile::$ITEM_CAR;
                        }
                    ?>

                    <?php if (array_key_exists('id_brand', $_GET) && $user->getPrivilege() == 1) {?>
                                <a href="<?php echo PathTools::makePathToProjectRoot() . PathPage::$PAGE_CREATE_CAR?>?idBrand=<?php echo $_GET['id_brand']?>" class="button-create p-2"
                                   style="align-self: center;">
                                    <img src="<?php echo PathTools::makePathToProjectRoot() . 'img/add.png'?>" style="width: 3rem; filter: invert(1);" alt="add" >
                                </a>
                    <?php }?>

                </div>
            </div>

        </div>

    </div>

</body>
</html>

