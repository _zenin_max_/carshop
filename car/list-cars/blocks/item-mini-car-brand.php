
<div class="<?php echo ($_GET['id_brand'] == $carBrand->getId()) ? 'select-car-brand' : 'car-brand';?> d-flex align-items-center"
   style="min-width: 15rem; ">
    <a href="<?php echo PathTools::makePathToProjectRoot() . PathPage::$PAGE_LIST_CAR . "?id_brand=" . $carBrand->getId(); ?>"
        class="d-flex align-items-center" style="width: <?php echo ($user->getPrivilege() == 1) ? 'calc(100% - 3rem)' : '100%'?>">
        <img class="p-2 m-1" src="<?php echo $carBrand->getPathLogo()?>" style="width: 3rem;" alt="Car brand">
        <h6 class="m-0" style="min-width: calc(100% - 3rem) ; text-align: center; font-weight: bold; height: min-content;">
            <?php echo $carBrand->getNameBrand()?>
        </h6>
    </a>

    <?php if ($user->getPrivilege() == 1) {?>
        <button form="formBrand<?php echo $carBrand->getId()?>" style="width: 1.5rem;" class="ml-1 mr-1">
            <img src="<?php echo PathTools::makePathToProjectRoot() . 'img/edit.png' ?>" style="width: 1.5rem; height: 1.5rem" alt="edit">
        </button>
        <button onclick="onClickDeleteBrand('<?php echo $carBrand->getId();?>')" style="width: 1.5rem;" class="ml-1 mr-1" ">
            <img src="<?php echo PathTools::makePathToProjectRoot() . 'img/delete.png' ?>" style="width: 1.5rem; height: 1.5rem" alt="delete">
        </button>
        <form id="formBrand<?php echo $carBrand->getId()?>" method="post" action="<?php echo PathTools::makePathToProjectRoot() . PathPage::$PAGE_UPDATE_BRAND?>">
            <input type="hidden" name="idBrand" value="<?php echo $carBrand->getId()?>">
        </form>
    <?php }?>
</div>


<script src="<?php echo PathFile::$JS_JQUERY?>"></script>
<script>
    function onClickDeleteBrand(idBrand) {
        let ob = {
            'idBrand': idBrand
        };
        jQuery.ajax({
            type: 'POST',
            url: '<?php echo PathFile::$METHOD_DELETE_BRAND?>',
            dataType: 'json',
            data: "json=" + JSON.stringify(ob),
            success: function (answer) {
                if (answer.<?php echo Status::$STATUS?> === '<?php echo Status::$SUCCESS ?>'){
                    console.log(answer.<?php echo Status::$STATUS?> + ': ' + answer.<?php echo Status::$MESSAGE?>);
                    document.location.href = "<?php echo PathTools::makePathToProjectRoot() . PathPage::$PAGE_LIST_CAR;?>";
                } else {
                    console.log(answer.<?php echo Status::$STATUS?> + ': ' + answer.<?php echo Status::$MESSAGE?>);
                }
            }
        });
    }
</script>
