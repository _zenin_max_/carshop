<!-- На вход подаётся: Car(class) -->

<?php

    require_once $_SERVER['DOCUMENT_ROOT'] . "/resource/path.php";
    require_once $_SERVER['DOCUMENT_ROOT'] . PathFile::$OBJECT_USER;
    require_once $_SERVER['DOCUMENT_ROOT'] . PathFile::$OBJECT_CAR;
    require_once $_SERVER['DOCUMENT_ROOT'] . PathFile::$OBJECT_CAR_BRAND;

    $user = new User( (array_key_exists ( 'userId' , $_SESSION )) ? $_SESSION['userId'] : '');
?>

<div id="divCar<?php echo $car->getId();?>" >
    <div class="card-grey d-flex-row card-grey-hv m-3 pl-3 pr-3 pt-0 pb-3" style="width: 16rem;" >
        <a href="<?php echo PathTools::makePathToProjectRoot() . PathPage::$PAGE_CAR_INFORMATION?>?id_car=<?php  echo $car->getId(); ?>" class="d-flex justify-content-center flex-wrap">
            <img class="radius-bottom " src="<?php echo $car->getListPhotoPath()[0]; ?>" style=" max-width: 100%; height: auto; width: auto; max-height: 8rem" alt="car" >
            <h5 class="mt-1" style="font-weight: bolder; text-align: center; width: 100%"> <?php echo $car->getNameCar(); ?> </h5>
        </a>

        <div class="d-flex flex-wrap justify-content-center card-in-card p-2 mt-1">
            <?php

                if ($user->getId()){
                    $listFavoriteCarsIdStr = $user->getFavoriteCars();
                    $listFavoriteCarsIdArr = explode(" ", $listFavoriteCarsIdStr);
                    $isFavoriteCar = in_array($car->getId(), $listFavoriteCarsIdArr);
                    if ($isFavoriteCar){
                        $setFavorite = 'false';
                        $srcImg = PathTools::makePathToProjectRoot() . 'img/favorite.png';
                    } else {
                        $setFavorite = 'true';
                        $srcImg = PathTools::makePathToProjectRoot() . 'img/no_favorite.png';
                    }

            ?>
                    <a href="<?php echo PathTools::makePathToProjectRoot() . PathFile::$METHOD_FAVORITE_CAR?>?id_car=<?php echo $car->getId(); ?>&set_favorite=<?php echo $setFavorite; ?>"
                       style="width: 1.5rem;" class="ml-1 mr-1">
                        <img src="<?php echo $srcImg; ?>" style="width: 100%;" alt="favorite">
                    </a>
                    <?php if ($user->getPrivilege() == 1) {?>
                        <button form="formCar<?php echo $car->getId()?>" style="width: 1.5rem;" class="ml-1 mr-1">
                            <img src="<?php echo PathTools::makePathToProjectRoot() . 'img/edit.png' ?>" style="width: 1.5rem; height: 1.5rem" alt="edit">
                        </button>
                        <button onclick="onClickDeleteCar('divCar<?php echo $car->getId();?>', '<?php echo $car->getId();?>')" style="width: 1.5rem;" class="ml-1 mr-1">
                            <img src="<?php echo PathTools::makePathToProjectRoot() . 'img/delete.png' ?>" style="width: 1.5rem; height: 1.5rem" alt="delete">
                        </button>
                        <form id="formCar<?php echo $car->getId()?>" method="post" action="<?php echo PathTools::makePathToProjectRoot() . PathPage::$PAGE_UPDATE_CAR?>">
                            <input type="hidden" name="idCar" value="<?php echo $car->getId()?>">
                        </form>
                    <?php }?>
            <?php } ?>
        </div>
    </div>
</div>


<script src="<?php echo PathFile::$JS_JQUERY?>"></script>
<script>
    function onClickDeleteCar(idItemCar, idCar) {
        let itemCar = document.getElementById(idItemCar);
        let ob = {
            'idCar': idCar
        };
        jQuery.ajax({
            type: 'POST',
            url: '<?php echo PathFile::$METHOD_DELETE_CAR?>',
            dataType: 'json',
            data: "json=" + JSON.stringify(ob),
            success: function (answer) {
                if (answer.<?php echo Status::$STATUS?> === '<?php echo Status::$SUCCESS ?>'){
                    itemCar.parentNode.removeChild(itemCar);
                    console.log(answer.<?php echo Status::$STATUS?> + ': ' + answer.<?php echo Status::$MESSAGE?>);
                } else {
                    console.log(answer.<?php echo Status::$STATUS?> + ': ' + answer.<?php echo Status::$MESSAGE?>);
                }
            }
        });
    }
</script>
