<?php

    class PathTools{
        static function makePathToProjectRoot(){
            $path = '';
            for ($i = 0; $i < substr_count($_SERVER['REQUEST_URI'], '/', 1) ; $i++){
                $path .= '../';
            }
            return  $path ;
        }
    }

    class PathFile {

        static $CALLBACK_OPERATION = '/callback/CallbackOperation.php';

        static $CSS_CARD = '/css/css-card.css';
        static $CSS_MAIN = '/css/css-main.css';
        static $CSS_CREATE_REVIEW = '/car/car-information/css/css-create-review.css';
        static $CSS_LIST_CAR = '/car/list-cars/css/css-list-car.css';
        static $CSS_BUTTON = '/css/css-button.css';

        static $OBJECT_SWEAR_WORD = '/objects/SwearWord.php';
        static $OBJECT_CAR = '/objects/Car.php';
        static $OBJECT_USER = '/objects/User.php';
        static $OBJECT_REVIEW_CAR = '/car/car-information/objects/ReviewCar.php';
        static $OBJECT_CAR_BRAND = '/objects/CarBrand.php';
        static $OBJECT_LIST_CAR_BRANDS = '/objects/ListCarBrands.php';
        static $OBJECT_LIST_CARS = '/objects/ListCars.php';
        static $OBJECT_LIST_REVIEWS_CAR = '/car/car-information/objects/ListReviewsCar.php';
        static $OBJECT_PASSWORD = '/objects/Password.php';
        static $OBJECT_SHOP = '/objects/Shop.php';
        static $OBJECT_LIST_SHOP = '/objects/ListShops.php';

        static $METHOD_FAVORITE_CAR = '/methods/method-favorite-car.php';
        static $METHOD_REGISTRATION = '/user/registration/methods/method-registration.php';
        static $METHOD_SIGN_OUT = '/user/profile/methods/method-sign-out.php';
        static $METHOD_SIGN_IN = '/user/sign-in/methods/method-sign-in.php';
        static $METHOD_DELETE_REVIEW_CAR = '/car/car-information/methods/method-delete-review-car.php';
        static $METHOD_CREATE_REVIEW_CAR = '/car/car-information/methods/method-create-review-car.php';
        static $METHOD_CREATE_CAR = '/car/create-car/methods/method-create-car.php';
        static $METHOD_DELETE_CAR = '/car/delete-car/methods/method-delete-car.php';
        static $METHOD_UPDATE_CAR = '/car/update-car/methods/method-update-car.php';
        static $METHOD_CREATE_BRAND = '/car/create-brand/methods/method-create-brand.php';
        static $METHOD_DELETE_BRAND = '/car/delete-brand/methods/method-delete-brand.php';
        static $METHOD_UPDATE_BRAND = '/car/update-brand/methods/method-update-brand.php';

        static $ITEM_CAR = '/car/list-cars/blocks/item-car.php';
        static $ITEM_CAR_BRAND = '/car/car-brands/blocks/item-car-brand.php';
        static $ITEM_IN_ITEM = '/car/car-information/blocks/item-in-item.php';
        static $ITEM_REVIEW = '/car/car-information/blocks/item-review.php';
        static $ITEM_FAVORITE_CAR = '/user/profile/blocks/item-favorite-car.php';
        static $ITEM_MAIN_MENU = '/main-screen/blocks/item-main-menu.php';
        static $ITEM_MINI_CAR_BRAND = '/car/list-cars/blocks/item-mini-car-brand.php';
        static $ITEM_TEXT_OPERATION = '/blocks/item-text-operation.php';

        static $BLOCK_BOOTSTRAP = '/blocks/block-bootstrap.php';
        static $BLOCK_HEADER = '/blocks/block-header.php';
        static $BLOCK_ABOUT_USER = '/user/profile/blocks/block-about-user.php';
        static $BLOCK_USER_FAVORITE_CARS = '/user/profile/blocks/block-user-favorite-cars.php';
        static $BLOCK_SLIDER_PHOTOS = '/blocks/block-slider-photos.php';
        static $BLOCK_REVIEWS_CAR = '/car/car-information/blocks/block-reviews-car.php';
        static $BLOCK_CREATE_REVIEW_CAR = '/car/car-information/blocks/block-create-review-car.php';
        static $BLOCK_ABOUT_CAR = '/car/car-information/blocks/block-about-car.php';

        static $JS_JQUERY = '/js/jquery-3.5.1.js';
        static $JS_UTILITY= '/js/js-utility.js';

        static $STRING= '/resource/string.php';

        static $TABLE_DB_CAR_BRANDS= '/database/Tables/TableDbCarBrands.php';
        static $TABLE_DB_USERS = '/database/Tables/TableDbUsers.php';
        static $TABLE_DB_CARS = '/database/Tables/TableDbCars.php';
        static $TABLE_DB_CATEGORIES = '/database/Tables/TableDbCategories.php';
        static $TABLE_DB_REMOVING_EXTRA_CHARACTERS = '/database/Tables/TableDbRemovingExtraCharacters.php';
        static $TABLE_DB_RUSSIAN_SWEAR_WORD = '/database/Tables/TableDbRussianSwearWord.php';
        static $TABLE_DB_RUSSIAN_LANGUAGE_PREFIXES = '/database/Tables/TableDbRussianLanguagePrefixes.php';
        static $TABLE_DB_REVIEWS_ABOUT_CAR = '/database/Tables/TableDbReviewsAboutCar.php';
        static $TABLE_DB_SHOPS = '/database/Tables/TableDbShops.php';

        static $CONNECT_DB_CARSHOP = '/database/ConnectDb/ConnectDbCarshop.php';
        static $CONNECT_DB_SWEAR_WORD = '/database/ConnectDb/ConnectDbSwearWord.php';

    }

    class PathPage {
        static $PAGE_LIST_CAR = 'car/list-cars/list-car.php';
        static $PAGE_SIGN_IN = 'user/sign-in/sign-in.php';
        static $PAGE_MAIN_SCREEN = 'main-screen/main-screen.php';
        static $PAGE_CAR_BRANDS = 'car/car-brands/car-brands.php';
        static $PAGE_PROFILE = 'user/profile/profile.php';
        static $PAGE_CAR_INFORMATION = 'car/car-information/car-information.php';
        static $PAGE_REGISTRATION = 'user/registration/registration.php';
        static $PAGE_SUCCESSFUL_REGISTRATION = 'user/successful-registration/successful-registration.php';
        static $PAGE_CREATE_BRAND = 'car/create-brand/create-brand.php';
        static $PAGE_CREATE_CAR = 'car/create-car/create-car.php';
        static $PAGE_UPDATE_CAR = 'car/update-car/update-car.php';
        static $PAGE_UPDATE_BRAND = 'car/update-brand/update-brand.php';
    }
    
?>