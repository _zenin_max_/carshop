<?php

    class Status{
        static $SUCCESS = 'SUCCESS';
        static $FAIL = 'FAIL';
        static $WRONG_FORMAT = 'WRONG_FORMAT';
        static $ERROR_DATABASE = 'ERROR_DATABASE';
        static $STATUS = 'status';
        static $MESSAGE = 'message';
    }